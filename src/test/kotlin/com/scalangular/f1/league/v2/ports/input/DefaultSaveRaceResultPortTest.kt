package com.scalangular.f1.league.v2.ports.input

import arrow.core.Either
import arrow.core.left
import com.scalangular.f1.league.v2.generateDriver
import com.scalangular.f1.league.v2.generateDriverRaceResult
import com.scalangular.f1.league.v2.generateLeague
import com.scalangular.f1.league.v2.generateRace
import com.scalangular.f1.league.v2.generated.transfer.entitys.toTransferModel
import com.scalangular.f1.league.v2.ports.input.SaveRaceResultPort.SaveRaceResultError
import com.scalangular.f1.league.v2.ports.input.SaveRaceResultPort.SaveRaceResultError.AlreadyExistingDriverResult
import com.scalangular.f1.league.v2.repositories.DriverRaceResultRepository
import com.scalangular.f1.league.v2.repositories.DriverRepository
import com.scalangular.f1.league.v2.repositories.RaceRepository
import io.kotest.assertions.arrow.core.shouldBeRight
import io.kotest.core.spec.style.StringSpec
import io.kotest.core.test.TestCase
import io.kotest.extensions.spring.SpringExtension
import io.kotest.matchers.shouldBe
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.cache.CacheManager
import java.util.*

@DataJpaTest
class DefaultSaveRaceResultPortTest(
    val entityManager: TestEntityManager,
    val cacheManager: CacheManager,
    driverRaceResultRepository: DriverRaceResultRepository,
    raceRepository: RaceRepository,
    driverRepository: DriverRepository,
) : StringSpec() {

    override fun beforeEach(testCase: TestCase) {
        super.beforeEach(testCase)
        entityManager.clear()
        entityManager.flush()
        cacheManager.getCache("driverRaceResults")?.clear()
        cacheManager.getCache("drivers")?.clear()
        cacheManager.getCache("races")?.clear()
    }

    override fun extensions() = listOf(SpringExtension)

    val saveRaceResultPort = DefaultSaveRaceResultPort(
        driverRaceResultRepository, raceRepository, driverRepository, cacheManager
    )

    init {
        "add results are saved as expected" {
            val league = generateLeague().copy(leagueId = null)
            val savedLeague = entityManager.persistAndFlush(league)
            val race = generateRace(savedLeague).copy(raceId = null)
            val drivers = (0..1).map { generateDriver(savedLeague).copy(driverId = null) }

            val savedRace = entityManager.persistAndFlush(race)
            val savedDrivers = drivers.map { entityManager.persistAndFlush(it) }

            val results = savedDrivers.mapIndexed { i, driver ->
                generateDriverRaceResult(driver, savedRace, i + 1).toTransferModel()
            }

            val result = saveRaceResultPort.saveRaceResults(savedRace.raceId!!, results)

            result.shouldBeRight()
        }

        "add results are not saved when already existent for a driver" {
            val league = generateLeague().copy(leagueId = null)
            val savedLeague = entityManager.persistAndFlush(league)
            val race = generateRace(savedLeague).copy(raceId = null)
            val drivers = (0..1).map { generateDriver(savedLeague).copy(driverId = null) }

            val savedRace = entityManager.persistAndFlush(race)
            val savedDrivers = drivers.map { entityManager.persistAndFlush(it) }

            val results = savedDrivers.mapIndexed { i, driver ->
                generateDriverRaceResult(driver, savedRace, i + 1)
            }

            entityManager.persistAndFlush(results.first().copy(driverRaceResultId = null))

            val result: Either<SaveRaceResultError, Unit> =
                saveRaceResultPort.saveRaceResults(race.raceId!!, results.map { it.toTransferModel() })

            val expected =
                AlreadyExistingDriverResult("For drivers [${drivers.first().driverId}] there is already a result for race ${race.raceId}").left()

            expected shouldBe result
        }

        "add results are not saved when race is not found" {
            val league = generateLeague().copy(leagueId = null)
            val savedLeague = entityManager.persistAndFlush(league)
            val race = generateRace(savedLeague)
            val drivers = (0..1).map { generateDriver(savedLeague).copy(driverId = null) }

            val savedDrivers = drivers.map { entityManager.persistAndFlush(it) }

            val results = savedDrivers.mapIndexed { i, driver ->
                generateDriverRaceResult(driver, race, i + 1)
            }

            val result: Either<SaveRaceResultError, Unit> =
                saveRaceResultPort.saveRaceResults(race.raceId!!, results.map { it.toTransferModel() })

            val expected =
                SaveRaceResultError.RaceNotFound(race.raceId!!).left()

            expected shouldBe result
        }

        "add results are not saved when unknown driver is encountered" {
            val league = generateLeague().copy(leagueId = null)
            val savedLeague = entityManager.persistAndFlush(league)
            val race = generateRace(savedLeague).copy(raceId = null)

            val drivers = (0..1).map { generateDriver(league).copy(driverId = null) }
            val driver2 = drivers.last().copy(driverId = UUID.randomUUID())
            val savedRace = entityManager.persistAndFlush(race)
            val savedDrivers = entityManager.persistAndFlush(drivers.first())
            entityManager.clear()

            val results = listOf(
                generateDriverRaceResult(savedDrivers, race, 1),
                generateDriverRaceResult(driver2, race, 1)
            )

            val result: Either<SaveRaceResultError, Unit> =
                saveRaceResultPort.saveRaceResults(savedRace.raceId!!, results.map { it.toTransferModel() })

            val expected =
                SaveRaceResultError.UnknownDriverEncountered(listOf(driver2.driverId!!)).left()

            expected shouldBe result
        }
    }
}
