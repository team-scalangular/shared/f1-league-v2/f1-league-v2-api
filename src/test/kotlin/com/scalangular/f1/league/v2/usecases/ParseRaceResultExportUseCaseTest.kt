package com.scalangular.f1.league.v2.usecases

import arrow.core.Either
import arrow.core.getOrElse
import arrow.core.left
import arrow.core.right
import com.scalangular.f1.league.v2.entities.F1Driver
import com.scalangular.f1.league.v2.entities.League
import com.scalangular.f1.league.v2.generated.transfer.entitys.DriverRaceResult
import com.scalangular.f1.league.v2.generated.transfer.entitys.F1RaceResultStatus
import com.scalangular.f1.league.v2.generated.transfer.entitys.RaceResult
import com.scalangular.f1.league.v2.usecases.parsing.DefaultCommonParsing
import com.scalangular.f1.league.v2.usecases.parsing.F12020ParseRaceResultExportUseCase
import com.scalangular.f1.league.v2.usecases.parsing.ParseRaceResultExportUseCase
import com.scalangular.f1.league.v2.usecases.parsing.ParseRaceResultExportUseCase.ParsingError
import io.kotest.core.spec.style.AnnotationSpec
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import java.io.File
import java.util.*

class ParseRaceResultExportUseCaseTest : AnnotationSpec() {

    private val commonParsing = DefaultCommonParsing()

    private val league = League(
        leagueName = "a",
        leagueId = null
    )

    private val parseRaceResultExportUseCaseF1: ParseRaceResultExportUseCase =
        F12020ParseRaceResultExportUseCase(commonParsing)

    private val singleJsonString: String = File("./src/test/resources/export.json").readText(Charsets.UTF_8)
    private val partialJsonString: String = File("./src/test/resources/export_partial.json").readText(Charsets.UTF_8)
    private val allJsonString: String = File("./src/test/resources/export_fully.json").readText(Charsets.UTF_8)

    @Test
    fun parseEmptyJson_NoJsonProvidedError() = runBlocking {
        val raceId = UUID.randomUUID()
        val export = ""

        val command = ParseRaceResultExportUseCase.ParseRaceResultCommand(
            raceId,
            emptyList(),
            export
        )

        assertEquals(ParsingError.ReceivedEmptyJson.left(), parseRaceResultExportUseCaseF1.parseRaceResult(command))
    }

    @Test
    fun parseMalformedJson_MalformedJsonError() = runBlocking {
        val raceId = UUID.randomUUID()
        val export = "]"

        val command = ParseRaceResultExportUseCase.ParseRaceResultCommand(
            raceId,
            emptyList(),
            export
        )

        assertEquals(
            ParsingError.MalformedJsonError(
                "Unexpected close marker ']': expected '}' (for root starting at [Source: (String)\"]\"; line: 1, column: 0])\n" +
                    " at [Source: (String)\"]\"; line: 1, column: 2]"
            ).left(),
            parseRaceResultExportUseCaseF1.parseRaceResult(command)
        )
    }

    @Test
    fun putRaceId_CorrectRaceId() = runBlocking {
        val raceId = UUID.randomUUID()

        val drivers = listOf(
            F1Driver(
                name = "Albon",
                driverId = UUID.randomUUID(),
                carNumber = 23,
                league = league,
                surename = null
            )
        )

        val command = ParseRaceResultExportUseCase.ParseRaceResultCommand(
            raceId,
            drivers,
            singleJsonString
        )

        val res = parseRaceResultExportUseCaseF1.parseRaceResult(command)

        assertEquals(raceId, res.map { it.raceId }.getOrElse { })
    }

    @Test
    fun emptyDriverList_CantMatchDriverError() = runBlocking {
        val raceId = UUID.randomUUID()

        val command = ParseRaceResultExportUseCase.ParseRaceResultCommand(
            raceId,
            emptyList(),
            singleJsonString
        )

        val res = parseRaceResultExportUseCaseF1.parseRaceResult(command)

        assertEquals(ParsingError.CantMatchDriverError("carNumber 23 cant be matched to a driver").left(), res)
    }

    @Test
    fun parseADriverResultWhereDriverCanBeMatched_CorrectlyParsed() = runBlocking {
        val raceId = UUID.randomUUID()

        val drivers = listOf(
            F1Driver(
                name = "Albon",
                driverId = UUID.randomUUID(),
                carNumber = 23,
                league = league,
                surename = null
            )
        )

        val command = ParseRaceResultExportUseCase.ParseRaceResultCommand(
            raceId,
            drivers,
            singleJsonString
        )

        val res = parseRaceResultExportUseCaseF1.parseRaceResult(command)
        val expected = RaceResult(
            raceId,
            listOf(
                DriverRaceResult(
                    raceId,
                    endPosition = 4,
                    drivenLaps = 3,
                    gridPosition = 4,
                    bestLapTime = 116.97769927978516f,
                    totalRaceTime = 355.2265319824219,
                    penaltiesAmount = 0,
                    penaltiesTime = 0f,
                    pitStops = 0,
                    driverId = drivers.first().driverId!!,
                    raceNumber = 23,
                    raceResultStatus = F1RaceResultStatus.finished
                )
            )
        )

        assertEquals(
            expected.right(),
            res.map {
                RaceResult(
                    it.raceId,
                    it.driverResults?.subList(0, 1)
                )
            }
        )
    }

    @Test
    fun parseADriverResult_CorrectlyParsed() = runBlocking {
        val raceId = UUID.randomUUID()

        val drivers = listOf(
            F1Driver(
                name = "Albon",
                driverId = UUID.randomUUID(),
                carNumber = 23,
                league = league,
                surename = null
            )
        )

        val command = ParseRaceResultExportUseCase.ParseRaceResultCommand(
            raceId,
            drivers,
            singleJsonString
        )

        val res = parseRaceResultExportUseCaseF1.parseRaceResult(command)
        val expected = RaceResult(
            raceId,
            listOf(
                DriverRaceResult(
                    raceId,
                    endPosition = 4,
                    drivenLaps = 3,
                    gridPosition = 4,
                    bestLapTime = 116.97769927978516f,
                    totalRaceTime = 355.2265319824219,
                    penaltiesAmount = 0,
                    penaltiesTime = 0f,
                    pitStops = 0,
                    driverId = drivers.first().driverId!!,
                    raceNumber = 23,
                    raceResultStatus = F1RaceResultStatus.finished
                )
            )
        )

        assertEquals(
            expected.right(),
            res.map {
                RaceResult(
                    it.raceId,
                    it.driverResults?.subList(0, 1)
                )
            }
        )
    }

    @Test
    fun parseWithArrayAtPointWhereItDoesntBelong_MalformedJsonError() = runBlocking {
        val raceId = UUID.randomUUID()
        val export = """  [{
    "m_position": [],
    "m_numLaps": 3,
    "m_gridPosition": 4,
    "m_points": 12,
    "m_numPitStops": 0,
    "m_resultStatus": 12,
    "m_bestLapTime": 116.97769927978516,
    "m_totalRaceTime": 355.2265319824219,
    "m_penaltiesTime": 0,
    "m_numPenalties": 0,
    "m_numTyreStints": 1,
    "m_aiControlled": 1,
    "m_driverId": 62,
    "m_teamId": 2,
    "m_raceNumber": 23,
    "m_nationality": 81,
    "m_name": "ALBON",
    "m_yourTelemetry": 1
  }]"""

        val drivers = listOf(
            F1Driver(
                name = "Albon",
                driverId = UUID.randomUUID(),
                carNumber = 23,
                league = league,
                surename = null
            )
        )

        val command = ParseRaceResultExportUseCase.ParseRaceResultCommand(
            raceId,
            drivers,
            export
        )

        assertEquals(
            ParsingError.MalformedJsonError(
                "Field m_position is a ARRAY not a value node. At {\"m_position\":[],\"m_numLaps\":3,\"m_gridPosition\":4,\"m_points\":12,\"m_numPitStops\":0,\"m_resultStatus\":12,\"m_bestLapTime\":116.97769927978516,\"m_totalRaceTime\":355.2265319824219,\"m_penaltiesTime\":0,\"m_numPenalties\":0,\"m_numTyreStints\":1,\"m_aiControlled\":1,\"m_driverId\":62,\"m_teamId\":2,\"m_raceNumber\":23,\"m_nationality\":81,\"m_name\":\"ALBON\",\"m_yourTelemetry\":1}"
            ).left(),
            parseRaceResultExportUseCaseF1.parseRaceResult(command)
        )
    }

    @Test
    fun parseMultipleDriverResultsWhereOneDriverCantBeMatched_CorrectlyParsed() = runBlocking {
        val raceId = UUID.randomUUID()

        val drivers = listOf(
            F1Driver(
                name = "Albon",
                driverId = UUID.randomUUID(),
                carNumber = 23,
                league = league,
                surename = null
            )
        )

        val command = ParseRaceResultExportUseCase.ParseRaceResultCommand(
            raceId,
            drivers,
            allJsonString
        )

        val res = parseRaceResultExportUseCaseF1.parseRaceResult(command)

        assertTrue(res.isLeft())
    }

    @Test
    fun `parse all driver results of a json`() = runBlocking {
        val raceId = UUID.randomUUID()

        val drivers = listOf(
            F1Driver(
                name = "D1",
                driverId = UUID.randomUUID(),
                carNumber = 23,
                league = league,
                surename = null
            ),
            F1Driver(
                name = "D2",
                driverId = UUID.randomUUID(),
                carNumber = 99,
                league = league,
                surename = null
            ),
            F1Driver(
                name = "D3",
                driverId = UUID.randomUUID(),
                carNumber = 11,
                league = league,
                surename = null
            ),
            F1Driver(
                name = "D4",
                driverId = UUID.randomUUID(),
                carNumber = 31,
                league = league,
                surename = null
            ),
            F1Driver(
                name = "D5",
                driverId = UUID.randomUUID(),
                carNumber = 20,
                league = league,
                surename = null
            ),
            F1Driver(
                name = "D6",
                driverId = UUID.randomUUID(),
                carNumber = 10,
                league = league,
                surename = null
            ),
            F1Driver(
                name = "D7",
                driverId = UUID.randomUUID(),
                carNumber = 7,
                league = league,
                surename = null
            ),
            F1Driver(
                name = "D8",
                driverId = UUID.randomUUID(),
                carNumber = 44,
                league = league,
                surename = null
            ),
            F1Driver(
                name = "D9",
                driverId = UUID.randomUUID(),
                carNumber = 33,
                league = league,
                surename = null
            ),

            F1Driver(
                name = "D10",
                driverId = UUID.randomUUID(),
                carNumber = 63,
                league = league,
                surename = null
            ),
            F1Driver(
                name = "D11",
                driverId = UUID.randomUUID(),
                carNumber = 5,
                league = league,
                surename = null
            ),
            F1Driver(
                name = "D12",
                driverId = UUID.randomUUID(),
                carNumber = 6,
                league = league,
                surename = null
            ),

            F1Driver(
                name = "D13",
                driverId = UUID.randomUUID(),
                carNumber = 18,
                league = league,
                surename = null
            ),
            F1Driver(
                name = "D14",
                driverId = UUID.randomUUID(),
                carNumber = 8,
                league = league,
                surename = null
            ),

            F1Driver(
                name = "D15",
                driverId = UUID.randomUUID(),
                carNumber = 55,
                league = league,
                surename = null
            ),

            F1Driver(
                name = "D16",
                driverId = UUID.randomUUID(),
                carNumber = 26,
                league = league,
                surename = null
            ),
            F1Driver(
                name = "D17",
                driverId = UUID.randomUUID(),
                carNumber = 4,
                league = league,
                surename = null
            ),
            F1Driver(
                name = "D18",
                driverId = UUID.randomUUID(),
                carNumber = 16,
                league = league,
                surename = null
            ),

            F1Driver(
                name = "D19",
                driverId = UUID.randomUUID(),
                carNumber = 77,
                league = league,
                surename = null
            ),
            F1Driver(
                name = "D20",
                driverId = UUID.randomUUID(),
                carNumber = 3,
                league = league,
                surename = null
            )
        )

        val command = ParseRaceResultExportUseCase.ParseRaceResultCommand(
            raceId,
            drivers,
            allJsonString
        )

        val res: Either<ParsingError, RaceResult> = parseRaceResultExportUseCaseF1.parseRaceResult(command)

        assertTrue(res.isRight())
    }

    @Test
    fun parseJsonWithInvalidRaceResult_InvalidFieldError() = runBlocking {
        val raceId = UUID.randomUUID()

        val drivers = listOf(
            F1Driver(
                name = "Albon",
                driverId = UUID.randomUUID(),
                carNumber = 23,
                league = league,
                surename = null
            )
        )

        val command = ParseRaceResultExportUseCase.ParseRaceResultCommand(
            raceId,
            drivers,
            """  [{
    "m_position": 4,
    "m_numLaps": 3,
    "m_gridPosition": 4,
    "m_points": 12,
    "m_numPitStops": 0,
    "m_resultStatus": 12,
    "m_bestLapTime": 116.97769927978516,
    "m_totalRaceTime": 355.2265319824219,
    "m_penaltiesTime": 0,
    "m_numPenalties": 0,
    "m_numTyreStints": 1,
    "m_aiControlled": 1,
    "m_driverId": 62,
    "m_teamId": 2,
    "m_raceNumber": 23,
    "m_nationality": 81,
    "m_name": "ALBON",
    "m_yourTelemetry": 1
  }]""".trim()
        )

        val res = parseRaceResultExportUseCaseF1.parseRaceResult(command)

        assertEquals(
            ParsingError.IllegalFieldStateError("Field m_resultStatus for driver with raceNumber 23 was 12 but should be >= 0 and <= 6")
                .left(),
            res
        )
    }

    @Test
    fun parsJsonWithResult_Containing0ForFastestLap_CreatesError() = runBlocking {
        val raceId = UUID.randomUUID()

        val drivers = listOf(
            F1Driver(
                name = "Albon",
                driverId = UUID.randomUUID(),
                carNumber = 23,
                league = league,
                surename = null
            )
        )

        val command = ParseRaceResultExportUseCase.ParseRaceResultCommand(
            raceId,
            drivers,
            """  [{
    "m_position": 1,
    "m_numLaps": 3,
    "m_gridPosition": 4,
    "m_points": 12,
    "m_numPitStops": 0,
    "m_resultStatus": 3,
    "m_bestLapTime": 0,
    "m_totalRaceTime": 355.2265319824219,
    "m_penaltiesTime": 0,
    "m_numPenalties": 0,
    "m_numTyreStints": 1,
    "m_aiControlled": 1,
    "m_driverId": 62,
    "m_teamId": 2,
    "m_raceNumber": 23,
    "m_nationality": 81,
    "m_name": "ALBON",
    "m_yourTelemetry": 1
  }]""".trim()
        )

        val res = parseRaceResultExportUseCaseF1.parseRaceResult(command)

        assertEquals(
            ParsingError.IllegalFieldStateError("Field m_bestLapTime is <= 0.0 for m_raceNumber 23").left(),
            res
        )

    }

    @Test
    fun parseJsonWithInvalidType_InvalidFieldError() = runBlocking {
        val raceId = UUID.randomUUID()

        val drivers = listOf(
            F1Driver(
                name = "Albon",
                driverId = UUID.randomUUID(),
                carNumber = 23,
                league = league,
                surename = null
            )
        )

        val command = ParseRaceResultExportUseCase.ParseRaceResultCommand(
            raceId,
            drivers,
            """  [{
    "m_position": abc,
    "m_numLaps": 3,
    "m_gridPosition": 4,
    "m_points": 12,
    "m_numPitStops": 0,
    "m_resultStatus": 3,
    "m_bestLapTime": 116.97769927978516,
    "m_totalRaceTime": 355.2265319824219,
    "m_penaltiesTime": 0,
    "m_numPenalties": 0,
    "m_numTyreStints": 1,
    "m_aiControlled": 1,
    "m_driverId": 62,
    "m_teamId": 2,
    "m_raceNumber": 23,
    "m_nationality": 81,
    "m_name": "ALBON",
    "m_yourTelemetry": 1
  }]""".trim()
        )

        val res = parseRaceResultExportUseCaseF1.parseRaceResult(command)

        assertEquals(
            ParsingError.MalformedJsonError(
                message = """Unrecognized token 'abc': was expecting (JSON String, Number, Array, Object or token 'null', 'true' or 'false')
 at [Source: (String)"[{
    "m_position": abc,
    "m_numLaps": 3,
    "m_gridPosition": 4,
    "m_points": 12,
    "m_numPitStops": 0,
    "m_resultStatus": 3,
    "m_bestLapTime": 116.97769927978516,
    "m_totalRaceTime": 355.2265319824219,
    "m_penaltiesTime": 0,
    "m_numPenalties": 0,
    "m_numTyreStints": 1,
    "m_aiControlled": 1,
    "m_driverId": 62,
    "m_teamId": 2,
    "m_raceNumber": 23,
    "m_nationality": 81,
    "m_name": "ALBON",
    "m_yourTelemetry": 1
  }]"; line: 2, column: 22]"""
            )
                .left(),
            res
        )
    }

    @Test
    fun parseJsonWithNoArray_InvalidFieldError() = runBlocking {
        val raceId = UUID.randomUUID()

        val drivers = listOf(
            F1Driver(
                name = "Albon",
                driverId = UUID.randomUUID(),
                carNumber = 23,
                league = league,
                surename = null
            )
        )

        val command = ParseRaceResultExportUseCase.ParseRaceResultCommand(
            raceId,
            drivers,
            """  {
    "m_position": 4,
    "m_numLaps": 3,
    "m_gridPosition": 4,
    "m_points": 12,
    "m_numPitStops": 0,
    "m_resultStatus": 12,
    "m_bestLapTime": 116.97769927978516,
    "m_totalRaceTime": 355.2265319824219,
    "m_penaltiesTime": 0,
    "m_numPenalties": 0,
    "m_numTyreStints": 1,
    "m_aiControlled": 1,
    "m_driverId": 62,
    "m_teamId": 2,
    "m_raceNumber": 23,
    "m_nationality": 81,
    "m_name": "ALBON",
    "m_yourTelemetry": 1
  }"""
        )

        val res = parseRaceResultExportUseCaseF1.parseRaceResult(command)

        assertEquals(
            ParsingError.MalformedJsonError("No array found at top level in export")
                .left(),
            res
        )
    }

    @Test
    fun parseWithInvalidEndPosition_InvalidFieldError() = runBlocking {
        val raceId = UUID.randomUUID()

        val drivers = listOf(
            F1Driver(
                name = "Albon",
                driverId = UUID.randomUUID(),
                carNumber = 23,
                league = league,
                surename = null
            )
        )

        val command = ParseRaceResultExportUseCase.ParseRaceResultCommand(
            raceId,
            drivers,
            """[{
    "m_position": 0,
    "m_numLaps": 3,
    "m_gridPosition": 4,
    "m_points": 12,
    "m_numPitStops": 0,
    "m_resultStatus": 4,
    "m_bestLapTime": 116.97769927978516,
    "m_totalRaceTime": 355.2265319824219,
    "m_penaltiesTime": 0,
    "m_numPenalties": 0,
    "m_numTyreStints": 1,
    "m_aiControlled": 1,
    "m_driverId": 62,
    "m_teamId": 2,
    "m_raceNumber": 23,
    "m_nationality": 81,
    "m_name": "ALBON",
    "m_yourTelemetry": 1
  }]"""
        )

        val res = parseRaceResultExportUseCaseF1.parseRaceResult(command)

        assertEquals(
            ParsingError.IllegalFieldStateError("Field m_position for driver with raceNumber 23 was 0 but should be >= 1 and <= 24")
                .left(),
            res
        )
    }

    @Test
    fun parseWithMissingField_MissingFieldError() = runBlocking {
        val raceId = UUID.randomUUID()

        val drivers = listOf(
            F1Driver(
                name = "Albon",
                driverId = UUID.randomUUID(),
                carNumber = 23,
                league = league,
                surename = null
            )
        )

        val command = ParseRaceResultExportUseCase.ParseRaceResultCommand(
            raceId,
            drivers,
            """[{
    "m_numLaps": 3,
    "m_gridPosition": 4,
    "m_points": 12,
    "m_numPitStops": 0,
    "m_resultStatus": 4,
    "m_bestLapTime": 116.97769927978516,
    "m_totalRaceTime": 355.2265319824219,
    "m_penaltiesTime": 0,
    "m_numPenalties": 0,
    "m_numTyreStints": 1,
    "m_aiControlled": 1,
    "m_driverId": 62,
    "m_teamId": 2,
    "m_raceNumber": 23,
    "m_nationality": 81,
    "m_name": "ALBON",
    "m_yourTelemetry": 1
  }]"""
        )

        val res = parseRaceResultExportUseCaseF1.parseRaceResult(command)

        assertEquals(
            ParsingError.MissingFieldError("Field m_position is missing. At {\"m_numLaps\":3,\"m_gridPosition\":4,\"m_points\":12,\"m_numPitStops\":0,\"m_resultStatus\":4,\"m_bestLapTime\":116.97769927978516,\"m_totalRaceTime\":355.2265319824219,\"m_penaltiesTime\":0,\"m_numPenalties\":0,\"m_numTyreStints\":1,\"m_aiControlled\":1,\"m_driverId\":62,\"m_teamId\":2,\"m_raceNumber\":23,\"m_nationality\":81,\"m_name\":\"ALBON\",\"m_yourTelemetry\":1}")
                .left(),
            res
        )
    }

    @Test
    fun parseResultSuccessfully_CorrectlyParsed() = runBlocking {
        val raceId = UUID.randomUUID()

        val albonId = UUID.randomUUID()
        val gioId = UUID.randomUUID()
        val perezId = UUID.randomUUID()

        val drivers = listOf(
            F1Driver(
                name = "Albon",
                driverId = albonId,
                carNumber = 23,
                league = league,
                surename = null
            ),
            F1Driver(
                name = "GIOVINAZZI",
                driverId = gioId,
                carNumber = 99,
                league = league,
                surename = null
            ),
            F1Driver(
                name = "PEREZ",
                driverId = perezId,
                carNumber = 11,
                league = league,
                surename = null
            )
        )

        val command = ParseRaceResultExportUseCase.ParseRaceResultCommand(
            raceId,
            drivers,
            partialJsonString
        )

        val res = parseRaceResultExportUseCaseF1.parseRaceResult(command)

        val expected = RaceResult(
            raceId,
            listOf(
                DriverRaceResult(
                    raceId,
                    endPosition = 4,
                    drivenLaps = 3,
                    gridPosition = 4,
                    bestLapTime = 116.97769927978516f,
                    totalRaceTime = 355.2265319824219,
                    penaltiesAmount = 0,
                    penaltiesTime = 0f,
                    pitStops = 0,
                    driverId = albonId,
                    raceNumber = 23,
                    raceResultStatus = F1RaceResultStatus.finished
                ),
                DriverRaceResult(
                    raceId,
                    endPosition = 18,
                    drivenLaps = 3,
                    gridPosition = 16,
                    bestLapTime = 119.43649291992188f,
                    totalRaceTime = 366.5439758300781,
                    penaltiesAmount = 0,
                    penaltiesTime = 0f,
                    pitStops = 1,
                    driverId = gioId,
                    raceNumber = 99,
                    raceResultStatus = F1RaceResultStatus.finished
                ),
                DriverRaceResult(
                    raceId,
                    endPosition = 8,
                    drivenLaps = 3,
                    gridPosition = 12,
                    bestLapTime = 117.06626892089844f,
                    totalRaceTime = 357.84381103515625,
                    penaltiesAmount = 1,
                    penaltiesTime = 1f,
                    pitStops = 0,
                    driverId = perezId,
                    raceNumber = 11,
                    raceResultStatus = F1RaceResultStatus.disqualified
                )
            )
        )

        assertEquals(
            expected.right(),
            res.map {
                RaceResult(
                    it.raceId,
                    it.driverResults
                )
            }
        )
    }

    @Test
    fun parseJsonWith0Values_AreIgnored() = runBlocking {
        val raceId = UUID.randomUUID()
        val export = File("./src/test/resources/export_partial_with_0_as_head.json").readText(Charsets.UTF_8)

        val albonId = UUID.randomUUID()
        val gioId = UUID.randomUUID()
        val perezId = UUID.randomUUID()

        val drivers = listOf(
            F1Driver(
                name = "Albon",
                driverId = albonId,
                carNumber = 23,
                league = league,
                surename = null
            ),
            F1Driver(
                name = "GIOVINAZZI",
                driverId = gioId,
                carNumber = 99,
                league = league,
                surename = null
            ),
            F1Driver(
                name = "PEREZ",
                driverId = perezId,
                carNumber = 11,
                league = league,
                surename = null
            )
        )

        val command = ParseRaceResultExportUseCase.ParseRaceResultCommand(
            raceId,
            drivers,
            export
        )

        val expected = RaceResult(
            raceId,
            listOf(
                DriverRaceResult(
                    raceId,
                    endPosition = 18,
                    drivenLaps = 3,
                    gridPosition = 16,
                    bestLapTime = 119.43649291992188f,
                    totalRaceTime = 366.5439758300781,
                    penaltiesAmount = 0,
                    penaltiesTime = 0f,
                    pitStops = 0,
                    driverId = gioId,
                    raceNumber = 99,
                    raceResultStatus = F1RaceResultStatus.finished
                ),
                DriverRaceResult(
                    raceId,
                    endPosition = 8,
                    drivenLaps = 3,
                    gridPosition = 12,
                    bestLapTime = 117.06626892089844f,
                    totalRaceTime = 357.84381103515625,
                    penaltiesAmount = 0,
                    penaltiesTime = 0f,
                    pitStops = 0,
                    driverId = perezId,
                    raceNumber = 11,
                    raceResultStatus = F1RaceResultStatus.finished
                )
            )
        )

        val result: Either<ParsingError, RaceResult> = parseRaceResultExportUseCaseF1.parseRaceResult(command)
        assertEquals(expected.right(), result)
    }
}
