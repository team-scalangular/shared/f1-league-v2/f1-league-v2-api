package com.scalangular.f1.league.v2.usecases

import arrow.core.left
import arrow.core.right
import com.scalangular.f1.league.v2.generated.transfer.entitys.DriverRaceResult
import com.scalangular.f1.league.v2.generated.transfer.entitys.F1RaceResultStatus
import com.scalangular.f1.league.v2.generated.transfer.entitys.RaceResult
import com.scalangular.f1.league.v2.generated.transfer.entitys.StandingRow
import com.scalangular.f1.league.v2.usecases.crud.GetStandingUseCase
import com.scalangular.f1.league.v2.usecases.crud.GetStandingUseCase.GetStandingCommand
import com.scalangular.f1.league.v2.usecases.crud.GetStandingUseCase.StandingError.NoRaceResults
import com.scalangular.f1.league.v2.usecases.crud.MemoizedF1StandingUseCase
import io.kotest.core.spec.style.AnnotationSpec
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import java.util.*

class GetStandingUseCaseTest : AnnotationSpec() {

    private val getClassificationUseCase: GetStandingUseCase = MemoizedF1StandingUseCase()

    private fun generateRaceResult(
        driverId: UUID,
        raceId: UUID,
        endPosition: Int,
        fastestLap: Float,
        raceResultStatus: F1RaceResultStatus
    ): DriverRaceResult {
        return DriverRaceResult(
            raceId,
            endPosition,
            70,
            2,
            fastestLap,
            6121.0,
            1,
            2f,
            1,
            driverId,
            11,
            raceResultStatus
        )
    }

    @Test
    fun noRaceResultsProvided_NoRaceResultsLeftError() = runBlocking {
        val command = GetStandingCommand(emptyList())
        assertEquals(NoRaceResults.left(), getClassificationUseCase.getStanding(command))
    }

    @Test
    fun raceResultWhereTwoDriversHaveTheFastestLap_NoExtraPointForBoth() = runBlocking {
        val raceId = UUID.randomUUID()
        val driver1 = UUID.randomUUID()
        val driver2 = UUID.randomUUID()
        val command = GetStandingCommand(
            listOf(
                RaceResult(
                    raceId,
                    listOf(
                        generateRaceResult(driver1, raceId, 1, 112.3f, F1RaceResultStatus.finished),
                        generateRaceResult(driver2, raceId, 2, 112.3f, F1RaceResultStatus.finished)
                    )
                )
            )
        )

        assertEquals(
            listOf(StandingRow(driver1, 1, 25), StandingRow(driver2, 2, 18)).right(),
            getClassificationUseCase.getStanding(command)
        )
    }

    @Test
    fun raceResultWithFastestLapForADriver_ExtraPointForDriver() = runBlocking {
        val raceId = UUID.randomUUID()
        val driver1 = UUID.randomUUID()
        val driver2 = UUID.randomUUID()
        val command = GetStandingCommand(
            listOf(
                RaceResult(
                    raceId,
                    listOf(
                        generateRaceResult(driver1, raceId, 1, 112f, F1RaceResultStatus.finished),
                        generateRaceResult(driver2, raceId, 2, 112.3f, F1RaceResultStatus.finished)
                    )
                )
            )
        )

        assertEquals(
            listOf(StandingRow(driver1, 1, 26), StandingRow(driver2, 2, 18)).right(),
            getClassificationUseCase.getStanding(command)
        )
    }

    @Test
    fun raceResultWithDriversOutsideOfTop10_NoPointsAndClassificationByEndPosition() = runBlocking {
        val raceId = UUID.randomUUID()
        val driver1 = UUID.randomUUID()
        val driver2 = UUID.randomUUID()
        val command = GetStandingCommand(
            listOf(
                RaceResult(
                    raceId,
                    listOf(
                        generateRaceResult(driver1, raceId, 20, 112f, F1RaceResultStatus.finished),
                        generateRaceResult(driver2, raceId, 11, 112.3f, F1RaceResultStatus.finished),
                    )
                )
            )
        )

        assertEquals(
            listOf(StandingRow(driver2, 1, 0), StandingRow(driver1, 2, 0)).right(),
            getClassificationUseCase.getStanding(command)
        )
    }

    @Test
    fun raceResultsWithDriversOutsideOfTop10AndSamePositionTwice_NoPointsAndClassificationByEndPosition() =
        runBlocking {
            val race1Id = UUID.randomUUID()
            val race2Id = UUID.randomUUID()
            val driver1 = UUID.randomUUID()
            val driver2 = UUID.randomUUID()
            val command = GetStandingCommand(
                listOf(
                    RaceResult(
                        race1Id,
                        listOf(
                            generateRaceResult(driver1, race1Id, 12, 112f, F1RaceResultStatus.finished),
                            generateRaceResult(driver2, race1Id, 11, 112.3f, F1RaceResultStatus.finished),
                        )
                    ),
                    RaceResult(
                        race1Id,
                        listOf(
                            generateRaceResult(driver1, race2Id, 11, 112f, F1RaceResultStatus.finished),
                            generateRaceResult(driver2, race2Id, 13, 112.3f, F1RaceResultStatus.finished),
                        )
                    )

                )
            )

            assertEquals(
                listOf(StandingRow(driver1, 1, 0), StandingRow(driver2, 2, 0)).right(),
                getClassificationUseCase.getStanding(command)
            )
        }

    @Test
    fun raceResultWithFastestLapOutsideOfTop10_NoExtraPointForTheDriverAndNoOtherFastestLap() = runBlocking {
        val raceId = UUID.randomUUID()
        val driver1 = UUID.randomUUID()
        val driver2 = UUID.randomUUID()
        val command = GetStandingCommand(
            listOf(
                RaceResult(
                    raceId,
                    listOf(
                        generateRaceResult(driver1, raceId, 12, 112f, F1RaceResultStatus.finished),
                        generateRaceResult(driver2, raceId, 2, 112.3f, F1RaceResultStatus.finished)
                    )
                )
            )
        )

        assertEquals(
            listOf(StandingRow(driver2, 1, 18), StandingRow(driver1, 2, 0)).right(),
            getClassificationUseCase.getStanding(command)
        )
    }

    @Test
    fun twoRacesWithADriverNotTakingPartInOne_ResultsPerPoints() = runBlocking {
        val race1Id = UUID.randomUUID()
        val race2Id = UUID.randomUUID()
        val driver1 = UUID.randomUUID()
        val driver2 = UUID.randomUUID()
        val driver3 = UUID.randomUUID()

        val race1Result = RaceResult(
            race1Id,
            listOf(
                generateRaceResult(driver1, race1Id, 2, 112f, F1RaceResultStatus.finished),
                generateRaceResult(driver2, race1Id, 4, 111.3f, F1RaceResultStatus.finished),
            )
        )

        val race2Result = RaceResult(
            race2Id,
            listOf(
                generateRaceResult(driver1, race2Id, 9, 112.1f, F1RaceResultStatus.finished),
                generateRaceResult(driver2, race2Id, 11, 112.3f, F1RaceResultStatus.finished),
                generateRaceResult(driver3, race2Id, 3, 110.3f, F1RaceResultStatus.finished)
            )
        )

        val command = GetStandingCommand(
            listOf(
                race1Result, race2Result
            )
        )

        assertEquals(
            listOf(
                StandingRow(driver1, 1, 20),
                StandingRow(driver3, 2, 16),
                StandingRow(driver2, 3, 13)
            ).right(),
            getClassificationUseCase.getStanding(command)
        )
    }

    @Test
    fun twoRaceResults_ClassificationAccordingToPointsByFinish() = runBlocking {
        val race1Id = UUID.randomUUID()
        val race2Id = UUID.randomUUID()
        val driver1 = UUID.randomUUID()
        val driver2 = UUID.randomUUID()

        val race1Result = RaceResult(
            race1Id,
            listOf(
                generateRaceResult(driver1, race1Id, 12, 112f, F1RaceResultStatus.finished),
                generateRaceResult(driver2, race1Id, 2, 112.3f, F1RaceResultStatus.finished)
            )
        )

        val race2Result = RaceResult(
            race2Id,
            listOf(
                generateRaceResult(driver1, race2Id, 12, 112f, F1RaceResultStatus.finished),
                generateRaceResult(driver2, race2Id, 2, 112.3f, F1RaceResultStatus.finished)
            )
        )

        val command = GetStandingCommand(
            listOf(
                race1Result, race2Result
            )
        )

        assertEquals(
            listOf(StandingRow(driver2, 1, 36), StandingRow(driver1, 2, 0)).right(),
            getClassificationUseCase.getStanding(command)
        )
    }

    @Test
    fun raceResultsWithInvalidResults_DontGetCountAndDontEffectFastestLap() = runBlocking {
        val race1Id = UUID.randomUUID()
        val race2Id = UUID.randomUUID()
        val driver1 = UUID.randomUUID()
        val driver2 = UUID.randomUUID()

        val race1Result = RaceResult(
            race1Id,
            listOf(
                generateRaceResult(driver1, race1Id, 2, 112f, F1RaceResultStatus.finished),
                generateRaceResult(driver2, race1Id, 4, 111.3f, F1RaceResultStatus.disqualified)
            )
        )

        val race2Result = RaceResult(
            race2Id,
            listOf(
                generateRaceResult(driver1, race2Id, 5, 112.1f, F1RaceResultStatus.invalid),
                generateRaceResult(driver2, race2Id, 1, 112.3f, F1RaceResultStatus.retired)
            )
        )

        val command = GetStandingCommand(
            listOf(
                race1Result, race2Result
            )
        )

        assertEquals(
            listOf(StandingRow(driver1, 1, 19), StandingRow(driver2, 2, 0)).right(),
            getClassificationUseCase.getStanding(command)
        )
    }

    @Test
    fun samePointsForMultipleDrivers_ClassificationForDriverWithBestResults() = runBlocking {
        val race1Id = UUID.randomUUID()
        val race2Id = UUID.randomUUID()
        val driver1 = UUID.randomUUID()
        val driver2 = UUID.randomUUID()

        // d1: p10 + fl = 2
        // d2: p8 = 4
        val race1Result = RaceResult(
            race1Id,
            listOf(
                generateRaceResult(driver1, race1Id, 10, 112f, F1RaceResultStatus.finished),
                generateRaceResult(driver2, race1Id, 8, 112.3f, F1RaceResultStatus.finished)
            )
        )

        // d1: 2 + p9 = 2 + 2 = 4
        // d2: 4 + p11 = 4 + 0 = 4
        val race2Result = RaceResult(
            race2Id,
            listOf(
                generateRaceResult(driver1, race2Id, 9, 112.5f, F1RaceResultStatus.finished),
                generateRaceResult(driver2, race2Id, 11, 112.3f, F1RaceResultStatus.finished)
            )
        )

        val race1Command = GetStandingCommand(
            listOf(
                race1Result
            )
        )

        val race1And2Command = GetStandingCommand(
            listOf(
                race1Result, race2Result
            )
        )

        assertEquals(
            listOf(StandingRow(driver2, 1, 4), StandingRow(driver1, 2, 2)).right(),
            getClassificationUseCase.getStanding(race1Command)
        )

        assertEquals(
            listOf(StandingRow(driver2, 1, 4), StandingRow(driver1, 2, 4)).right(),
            getClassificationUseCase.getStanding(race1And2Command)
        )
    }

    @Test
    fun differentPointMaps_ReturnsDifferentClassificationPoints() = runBlocking {
        val raceId = UUID.randomUUID()
        val driver1 = UUID.randomUUID()
        val driver2 = UUID.randomUUID()
        val command = GetStandingCommand(
            listOf(
                RaceResult(
                    raceId,
                    listOf(
                        generateRaceResult(driver1, raceId, 12, 112.3f, F1RaceResultStatus.finished),
                        generateRaceResult(driver2, raceId, 11, 112.3f, F1RaceResultStatus.finished),
                    )
                )
            )
        )

        val pointsByPosition = mapOf(11 to 2, 12 to 4)

        assertEquals(
            listOf(StandingRow(driver1, 1, 4), StandingRow(driver2, 2, 2)).right(),
            MemoizedF1StandingUseCase(pointsByPosition).getStanding(command)
        )
    }
}
