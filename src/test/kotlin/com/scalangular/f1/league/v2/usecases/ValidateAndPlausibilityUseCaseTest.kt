package com.scalangular.f1.league.v2.usecases

import com.scalangular.f1.league.v2.usecases.parsing.DefaultCommonParsing
import com.scalangular.f1.league.v2.usecases.parsing.DefaultValidateAndPlausibilityUseCase
import com.scalangular.f1.league.v2.usecases.parsing.ValidateAndPlausibilityUseCase
import com.scalangular.f1.league.v2.usecases.parsing.ValidateAndPlausibilityUseCase.ValidateUseCaseCommand
import com.scalangular.f1.league.v2.usecases.parsing.ValidateAndPlausibilityUseCase.ValidationAndPlausibilityResult
import io.kotest.assertions.assertSoftly
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import java.io.File

class ValidateAndPlausibilityUseCaseTest : StringSpec() {

    private val export: String = File("./src/test/resources/f1-results-2021-6-1-SPA.json").readText(Charsets.UTF_8)
    private val allJsonString: String = File("./src/test/resources/export_fully.json").readText(Charsets.UTF_8)

    val commonParsing: com.scalangular.f1.league.v2.usecases.CommonParsing = DefaultCommonParsing()

    private val validateUseCase: ValidateAndPlausibilityUseCase = DefaultValidateAndPlausibilityUseCase(commonParsing)

    init {
        "car number occurs multiple times returns false and message" {
            val command = ValidateUseCaseCommand(
                export
            )

            val result = validateUseCase.validateAndPlausibilityCheckExport(command)
            val expected = ValidationAndPlausibilityResult(
                valid = false,
                messages = listOf("the m_raceNumber(s) [66, 12] occurred multiple times")
            )

            expected shouldBe result
        }

        "no car number multiple times results in true" {
            val command = ValidateUseCaseCommand(
                allJsonString
            )

            val result = validateUseCase.validateAndPlausibilityCheckExport(command)
            val expected = ValidationAndPlausibilityResult(
                valid = true,
                messages = emptyList()
            )

            expected shouldBe result
        }

        "not parsable json results in false with message" {
            val command = ValidateUseCaseCommand(
                "]"
            )

            val result = validateUseCase.validateAndPlausibilityCheckExport(command)
            val expected = ValidationAndPlausibilityResult(
                valid = false,
                messages = listOf("Couldn't parse json: ")
            )

            assertSoftly(result) {
                valid.shouldBeFalse()
                messages.first() shouldContain expected.messages.first()
            }
        }

        "empty json results in false with message" {
            val command = ValidateUseCaseCommand(
                ""
            )

            val result = validateUseCase.validateAndPlausibilityCheckExport(command)
            val expected = ValidationAndPlausibilityResult(
                valid = false,
                messages = listOf("Received empty json")
            )

            assertSoftly(result) {
                valid.shouldBeFalse()
                messages.first() shouldContain expected.messages.first()
            }
        }
    }
}
