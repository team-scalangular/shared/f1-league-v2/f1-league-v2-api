package com.scalangular.f1.league.v2.etc

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.net.URI

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension::class)
@ActiveProfiles(profiles = arrayOf("test"))
class EndpointTests {

    var testRestTemplate = TestRestTemplate()

    @LocalServerPort
    var port: Int = 0

    @Test
    fun metricsTest() {
        val result: ResponseEntity<Void> = testRestTemplate.exchange(
            URI("http://localhost:$port/metrics"),
            HttpMethod.GET,
            HttpEntity(""),
            Void::class.java)

        Assertions.assertEquals(HttpStatus.OK, result.statusCode)
    }
}