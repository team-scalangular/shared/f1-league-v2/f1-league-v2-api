package com.scalangular.f1.league.v2

import com.scalangular.f1.league.v2.entities.DriverRaceResult
import com.scalangular.f1.league.v2.entities.F1Driver
import com.scalangular.f1.league.v2.entities.League
import com.scalangular.f1.league.v2.entities.Race
import com.scalangular.f1.league.v2.generated.transfer.entitys.F1Circuit
import com.scalangular.f1.league.v2.generated.transfer.entitys.F1RaceResultStatus
import java.time.LocalDateTime
import java.util.*

fun generateLeague(): League {
    return League(
        "TestLeague",
        UUID.randomUUID()
    )
}

fun generateDriverRaceResult(
    driver: F1Driver,
    race: Race,
    endPosition: Int
): DriverRaceResult {
    return DriverRaceResult(
        UUID.randomUUID(),
        race,
        driver,
        endPosition,
        2,
        1,
        2f,
        0.0,
        1,
        0f,
        2,
        3,
        F1RaceResultStatus.finished
    )
}

fun generateDriver(league: League): F1Driver = F1Driver(
    "Test",
    " Driver",
    3,
    UUID.randomUUID(),
    league
)

fun generateRace(league: League): Race {
    return Race(
        UUID.randomUUID(),
        F1Circuit.ABUDHABI,
        racePositionIndex = 2,
        raceDate = LocalDateTime.now(),
        league
    )
}
