package com.scalangular.f1.league.v2.usecases.common

import com.scalangular.f1.league.v2.entities.DriverRaceResult
import com.scalangular.f1.league.v2.generateDriver
import com.scalangular.f1.league.v2.generateDriverRaceResult
import com.scalangular.f1.league.v2.generateLeague
import com.scalangular.f1.league.v2.generateRace
import com.scalangular.f1.league.v2.usecases.common.BuildRaceResultsUseCase.BuildRaceResultCommand
import io.kotest.assertions.assertSoftly
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Assertions.*

class DefaultBuildRaceResultsUseCaseTest : StringSpec() {
    private val buildRaceResultsUseCase = DefaultBuildRaceResultsUseCase()

    private val league = generateLeague()

    init {
        "no driver results results in empty race result" {
            val race = generateRace(league)

            val driverResults = emptyList<DriverRaceResult>()

            val result = buildRaceResultsUseCase.buildRaceResult(
                BuildRaceResultCommand(
                    race,
                    driverResults
                )
            )

            assertNotNull(result.driverResults)
            assertTrue(result.driverResults!!.isEmpty())
            assertEquals(race.raceId, result.raceId)

            assertSoftly(result) {
                driverResults.shouldNotBeNull()
                driverResults.shouldBeEmpty()
                raceId shouldBe race.raceId
            }
        }

        "with driver results should be added to raceresult" {
            val race = generateRace(league)
            val driver1 = generateDriver(league)
            val driver2 = generateDriver(league)
            val driverResults = listOf<DriverRaceResult>(
                generateDriverRaceResult(driver1, race, 1),
                generateDriverRaceResult(driver2, race, 2)
            )

            val result = buildRaceResultsUseCase.buildRaceResult(
                BuildRaceResultCommand(
                    race,
                    driverResults
                )
            )

            assertNotNull(result.driverResults)
            assertTrue((result.driverResults?.size ?: 0) == 2)
            assertEquals(race.raceId, result.raceId)

            assertSoftly(result) {
                driverResults.shouldNotBeNull()
                driverResults shouldHaveSize 2
                raceId shouldBe race.raceId
            }
        }
    }
}
