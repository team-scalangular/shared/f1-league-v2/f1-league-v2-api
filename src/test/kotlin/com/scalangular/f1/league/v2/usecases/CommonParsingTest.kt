package com.scalangular.f1.league.v2.usecases

import com.fasterxml.jackson.databind.ObjectMapper
import com.scalangular.f1.league.v2.usecases.parsing.DefaultCommonParsing
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import java.io.File

class CommonParsingTest : StringSpec() {

    private val commonParsingImpl = DefaultCommonParsing()

    init {
        "all 0 values results in false" {
            val all0Values = File("./src/test/resources/all0.json").readText(Charsets.UTF_8)

            val result = commonParsingImpl.isNonEmptyResult(ObjectMapper().readTree(all0Values))

            result.shouldBeFalse()
        }

        "all but 1 value results in true" {
            val all0Values = File("./src/test/resources/allBut1Is0.json").readText(Charsets.UTF_8)

            val result = commonParsingImpl.isNonEmptyResult(ObjectMapper().readTree(all0Values))

            result.shouldBeTrue()
        }
    }
}
