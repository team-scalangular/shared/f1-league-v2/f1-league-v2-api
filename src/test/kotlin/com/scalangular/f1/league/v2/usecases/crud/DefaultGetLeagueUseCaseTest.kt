package com.scalangular.f1.league.v2.usecases.crud

import arrow.core.*
import com.scalangular.f1.league.v2.entities.League
import com.scalangular.f1.league.v2.ports.output.GetLeaguePort
import com.scalangular.f1.league.v2.usecases.crud.GetLeagueUseCase.GetLeagueCommand
import com.scalangular.f1.league.v2.usecases.crud.GetLeagueUseCase.GetLeagueError
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import java.util.*

class DefaultGetLeagueUseCaseTest : StringSpec() {

    val leagueId: UUID = UUID.randomUUID()
    private val randomLeaguePort = object : GetLeaguePort {
        override fun getLeague(id: GetLeaguePort.GetLeagueCommand): Option<League> {
            return Some(
                League(
                    "MyLeague",
                    leagueId
                )
            )
        }
    }

    private val noneLeaguePort = object : GetLeaguePort {
        override fun getLeague(id: GetLeaguePort.GetLeagueCommand): Option<League> {
            return None
        }
    }

    init {
        "get league returns league as expected" {
            val useCase = DefaultGetLeagueUseCase(
                randomLeaguePort
            )

            val actual = useCase.getLeague(GetLeagueCommand(leagueId))
            val expected = League("MyLeague", leagueId).right()

            actual shouldBe expected
        }

        "get league but is not known should return league as expected" {
            val useCase = DefaultGetLeagueUseCase(
                noneLeaguePort
            )

            val actual = useCase.getLeague(GetLeagueCommand(leagueId))
            val expected = GetLeagueError.LeagueNotFound.left()

            expected shouldBe actual
        }
    }
}
