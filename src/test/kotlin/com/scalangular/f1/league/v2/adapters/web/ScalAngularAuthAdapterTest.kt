package com.scalangular.f1.league.v2.adapters.web

import com.scalangular.f1.league.v2.adapters.web.auth.AuthWebClint
import com.scalangular.f1.league.v2.adapters.web.auth.ScalAngularAuthAdapter
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.mock.http.server.reactive.MockServerHttpRequest
import org.springframework.mock.web.server.MockServerWebExchange
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.server.HandlerStrategies
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.buildAndAwait

class ScalAngularAuthAdapterTest : StringSpec({

    val alwaysTrueWebClient = object : AuthWebClint {
        override suspend fun authorize(url: String, token: String, resource: String, level: Int): ClientResponse {
            return ClientResponse.create(HttpStatus.OK).build()
        }
    }

    val alwaysFalseWebClient = object : AuthWebClint {
        override suspend fun authorize(url: String, token: String, resource: String, level: Int): ClientResponse =
            ClientResponse.create(HttpStatus.UNAUTHORIZED).build()
    }

    "when no token is provided the request is unauthorized" {
        val request: MockServerHttpRequest = MockServerHttpRequest.post("http://example.com")
            .header("foo", "bar")
            .build()
        val exchange = MockServerWebExchange.from(request)
        val other: ServerRequest = ServerRequest.create(exchange, HandlerStrategies.withDefaults().messageReaders())

        val result = ScalAngularAuthAdapter("https://www.google.com", alwaysTrueWebClient).authorizeRequest(
            other, "Test", 1
        ) {
            ServerResponse.ok().buildAndAwait()
        }

        result.statusCode() shouldBe HttpStatus.UNAUTHORIZED
    }

    "when header is present but empty the request is unauthorized" {
        val request: MockServerHttpRequest = MockServerHttpRequest.post("http://example.com")
            .header(HttpHeaders.AUTHORIZATION, "")
            .build()
        val exchange = MockServerWebExchange.from(request)
        val other: ServerRequest = ServerRequest.create(exchange, HandlerStrategies.withDefaults().messageReaders())

        val result = ScalAngularAuthAdapter("www.google.com", alwaysTrueWebClient).authorizeRequest(
            other, "Test", 1
        ) {
            ServerResponse.ok().buildAndAwait()
        }

        result.statusCode() shouldBe HttpStatus.UNAUTHORIZED
    }

    "when token is invalid the request is unauthorized" {
        val request: MockServerHttpRequest = MockServerHttpRequest.post("http://example.com")
            .header(
                HttpHeaders.AUTHORIZATION,
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
            )
            .build()
        val exchange = MockServerWebExchange.from(request)
        val other: ServerRequest = ServerRequest.create(exchange, HandlerStrategies.withDefaults().messageReaders())

        val result = ScalAngularAuthAdapter("www.google.com", alwaysFalseWebClient).authorizeRequest(
            other, "Test", 1
        ) {
            ServerResponse.ok().buildAndAwait()
        }

        result.statusCode() shouldBe HttpStatus.UNAUTHORIZED
    }

    "when token is valid the request is authorized" {
        val request: MockServerHttpRequest = MockServerHttpRequest.post("http://example.com")
            .header(
                HttpHeaders.AUTHORIZATION,
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
            )
            .build()
        val exchange = MockServerWebExchange.from(request)
        val other: ServerRequest = ServerRequest.create(exchange, HandlerStrategies.withDefaults().messageReaders())

        val result = ScalAngularAuthAdapter("www.google.com", alwaysTrueWebClient).authorizeRequest(
            other, "Test", 1
        ) {
            ServerResponse.ok().buildAndAwait()
        }

        result.statusCode() shouldBe HttpStatus.OK
    }
})
