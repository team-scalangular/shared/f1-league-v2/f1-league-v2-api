package com.scalangular.f1.league.v2.adapters

import com.scalangular.f1.league.v2.adapters.persitence.PersistenceAdapter
import com.scalangular.f1.league.v2.entities.F1Driver
import com.scalangular.f1.league.v2.entities.League
import com.scalangular.f1.league.v2.ports.output.DefaultLeagueExistsPort
import com.scalangular.f1.league.v2.ports.output.GetLeaguePort.GetLeagueCommand
import com.scalangular.f1.league.v2.repositories.DriverRepository
import com.scalangular.f1.league.v2.repositories.LeagueRepository
import com.scalangular.f1.league.v2.usecases.crud.GetDriversUseCase.GetDriversCommand
import com.scalangular.f1.league.v2.usecases.crud.GetDriversUseCase.GetDriversError.LeagueNotFoundError
import io.kotest.assertions.arrow.core.shouldBeLeft
import io.kotest.assertions.arrow.core.shouldBeRight
import io.kotest.assertions.arrow.core.shouldBeSome
import io.kotest.core.spec.style.StringSpec
import io.kotest.core.test.TestCase
import io.kotest.extensions.spring.SpringExtension
import io.kotest.matchers.shouldBe
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import java.util.*
import kotlin.random.Random

@DataJpaTest
class PersistenceAdapterTest(
    val entityManager: TestEntityManager,
    leagueRepository: LeagueRepository,
    driverRepository: DriverRepository,
) : StringSpec({
    val persistenceAdapter = PersistenceAdapter(
        leagueRepository,
        driverRepository,
        DefaultLeagueExistsPort(leagueRepository),
    )

    fun generateDriver(league: League): F1Driver {
        return F1Driver(
            driverId = null,
            name = "Moritz",
            surename = "Lindner",
            carNumber = Random(12131).nextInt(),
            league = league
        )
    }

    "get a league by id returns the league" {
        val league = League(
            "myLeague",
            null
        )
        val saved: League = entityManager.persistAndFlush(league)

        val res = persistenceAdapter.getLeague(GetLeagueCommand(saved.leagueId ?: UUID.randomUUID()))

        res.shouldBeSome().shouldBe(saved)
    }

    "get a leagues drivers" {
        val league = League(
            "myLeague",
            null
        )
        val savedLeague: League = entityManager.persistAndFlush(league)
        val drivers = List(100) {
            val driver = generateDriver(savedLeague)
            entityManager.persistAndFlush(driver)
        }

        val receivedDrivers = persistenceAdapter.getDrivers(GetDriversCommand(savedLeague.leagueId!!))

        receivedDrivers.shouldBeRight().shouldBe(drivers)
    }

    "getting drivers from a league that doesnt exist results in LeagueNotFoundError" {
        val expected = LeagueNotFoundError
        val actual = persistenceAdapter.getDrivers(GetDriversCommand(UUID.randomUUID()))

        actual.shouldBeLeft().shouldBe(expected)
    }
}) {
    override fun beforeEach(testCase: TestCase) {
        super.beforeEach(testCase)
        entityManager.clear()
    }

    override fun extensions() = listOf(SpringExtension)
}
