package com.scalangular.f1.league.v2.usecases.crud

import arrow.core.right
import com.scalangular.f1.league.v2.adapters.persitence.Mapper
import com.scalangular.f1.league.v2.entities.Race
import com.scalangular.f1.league.v2.generateLeague
import com.scalangular.f1.league.v2.generateRace
import com.scalangular.f1.league.v2.repositories.RaceRepository
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import java.util.*

class DefaultGetRacesUseCaseTest : StringSpec({
    val league = generateLeague()

    val racesList = listOf(
        generateRace(league).copy(racePositionIndex = 10),
        generateRace(league).copy(racePositionIndex = 1)
    )


    val getRacesUseCase = DefaultGetRacesUseCase(
        object : RaceRepository{
            override fun getRacesByLeagueLeagueId(leagueId: UUID): List<Race> {
                return racesList
            }

            override fun getRaceByRaceId(raceId: UUID): Race? {
                TODO("Not yet implemented")
            }

            override fun <S : Race?> save(entity: S): S {
                TODO("Not yet implemented")
            }

            override fun <S : Race?> saveAll(entities: MutableIterable<S>): MutableIterable<S> {
                TODO("Not yet implemented")
            }

            override fun findById(id: UUID): Optional<Race> {
                TODO("Not yet implemented")
            }

            override fun existsById(id: UUID): Boolean {
                TODO("Not yet implemented")
            }

            override fun findAll(): MutableIterable<Race> {
                TODO("Not yet implemented")
            }

            override fun findAllById(ids: MutableIterable<UUID>): MutableIterable<Race> {
                TODO("Not yet implemented")
            }

            override fun count(): Long {
                TODO("Not yet implemented")
            }

            override fun deleteById(id: UUID) {
                TODO("Not yet implemented")
            }

            override fun delete(entity: Race) {
                TODO("Not yet implemented")
            }

            override fun deleteAllById(ids: MutableIterable<UUID>) {
                TODO("Not yet implemented")
            }

            override fun deleteAll(entities: MutableIterable<Race>) {
                TODO("Not yet implemented")
            }

            override fun deleteAll() {
                TODO("Not yet implemented")
            }
        }
    )

    "returns races ordered by raceposition index (acd)"{
        val res = getRacesUseCase.getRaces(GetRacesUseCase.GetRacesCommand(league.leagueId!!))

        res shouldBe Mapper.map(racesList).reversed().right()
    }
})