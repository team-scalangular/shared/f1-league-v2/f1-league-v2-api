package com.scalangular.f1.league.v2.usecases.common

import arrow.core.Either
import arrow.core.right
import com.scalangular.f1.league.v2.generated.transfer.entitys.F1Circuit
import com.scalangular.f1.league.v2.generated.transfer.entitys.Race
import com.scalangular.f1.league.v2.usecases.common.GetALeaguesNextRaceUseCase.GetALeaguesNextRaceError.NoNextRace
import com.scalangular.f1.league.v2.usecases.crud.GetRacesUseCase
import io.kotest.assertions.arrow.core.shouldBeLeft
import io.kotest.assertions.arrow.core.shouldBeRight
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import java.time.LocalDate
import java.util.*

class GetALeaguesNextRaceUseCaseTest : StringSpec(
    {
        "correct race has same date index" {
            val races = listOf(
                Race(
                    circuit = F1Circuit.BAHRAIN,
                    raceId = UUID.randomUUID(),
                    racePositionIndex = 2,
                    date = LocalDate.now().plusDays(2)
                ),
                Race(
                    circuit = F1Circuit.AUSTRALIA,
                    raceId = UUID.randomUUID(),
                    racePositionIndex = 1,
                    date = LocalDate.now()
                ),
                Race(
                    circuit = F1Circuit.ABUDHABI,
                    raceId = UUID.randomUUID(),
                    racePositionIndex = 0,
                    date = LocalDate.now()
                )
            )

            val useCase: GetALeaguesNextRaceUseCase = DefaultGetALeaguesNextRaceUseCase(object : GetRacesUseCase {
                override fun getRaces(command: GetRacesUseCase.GetRacesCommand): Either<GetRacesUseCase.GetRacesError, List<Race>> {
                    return races.right()
                }
            })

            val race: Either<GetALeaguesNextRaceUseCase.GetALeaguesNextRaceError, Race> =
                useCase.getNextRaceForLeague(GetALeaguesNextRaceUseCase.GetALeaguesNextRaceCommand(UUID.randomUUID()))

            race.shouldBeRight().shouldBe(races[2])
        }

        "correct race with different date" {
            val races = listOf(
                Race(
                    circuit = F1Circuit.BAHRAIN,
                    raceId = UUID.randomUUID(),
                    racePositionIndex = 2,
                    date = LocalDate.now().plusDays(2)
                ),
                Race(
                    circuit = F1Circuit.AUSTRALIA,
                    raceId = UUID.randomUUID(),
                    racePositionIndex = 1,
                    date = LocalDate.now().plusDays(1)
                )
            )
            val useCase: GetALeaguesNextRaceUseCase = DefaultGetALeaguesNextRaceUseCase(object : GetRacesUseCase {
                override fun getRaces(command: GetRacesUseCase.GetRacesCommand): Either<GetRacesUseCase.GetRacesError, List<Race>> {
                    return races.right()
                }
            })

            val race: Either<GetALeaguesNextRaceUseCase.GetALeaguesNextRaceError, Race> =
                useCase.getNextRaceForLeague(GetALeaguesNextRaceUseCase.GetALeaguesNextRaceCommand(UUID.randomUUID()))

            race.shouldBeRight().shouldBe(races[1])
        }

        "correct date with dates in past are ignored" {
            val races = listOf(
                Race(
                    circuit = F1Circuit.BAHRAIN,
                    raceId = UUID.randomUUID(),
                    racePositionIndex = 2,
                    date = LocalDate.now().minusDays(1)
                ),
                Race(
                    circuit = F1Circuit.AUSTRALIA,
                    raceId = UUID.randomUUID(),
                    racePositionIndex = 1,
                    date = LocalDate.now().minusDays(2)
                )
            )

            val useCase: GetALeaguesNextRaceUseCase = DefaultGetALeaguesNextRaceUseCase(object : GetRacesUseCase {
                override fun getRaces(command: GetRacesUseCase.GetRacesCommand): Either<GetRacesUseCase.GetRacesError, List<Race>> {
                    return races.right()
                }
            })

            val race: Either<GetALeaguesNextRaceUseCase.GetALeaguesNextRaceError, Race> =
                useCase.getNextRaceForLeague(GetALeaguesNextRaceUseCase.GetALeaguesNextRaceCommand(UUID.randomUUID()))

            race.shouldBeLeft().shouldBe(NoNextRace)
        }
    }
)
