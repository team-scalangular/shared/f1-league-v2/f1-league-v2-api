package com.scalangular.f1.league.v2.usecases.parsing

import arrow.core.Validated
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.scalangular.f1.league.v2.usecases.CommonParsing
import com.scalangular.f1.league.v2.usecases.parsing.ValidateAndPlausibilityUseCase.ValidateUseCaseCommand
import com.scalangular.f1.league.v2.usecases.parsing.ValidateAndPlausibilityUseCase.ValidationAndPlausibilityResult

class DefaultValidateAndPlausibilityUseCase(private val commonParsing: CommonParsing) : ValidateAndPlausibilityUseCase {

    override fun validateAndPlausibilityCheckExport(command: ValidateUseCaseCommand): ValidationAndPlausibilityResult {
        return Validated.catch { ObjectMapper().readTree(command.export) }
            .fold(
                { error: Throwable ->
                    ValidationAndPlausibilityResult(
                        valid = false,
                        messages = listOf("Couldn't parse json: ${error.message}")
                    )
                },
                { node: JsonNode? ->
                    if (node != null && !node.isEmpty) {
                        return checkForMultipleNumbers(node).fold(
                            { x: String -> ValidationAndPlausibilityResult(false, listOf(x)) },
                            { ValidationAndPlausibilityResult(true, emptyList()) }
                        )
                    } else {
                        ValidationAndPlausibilityResult(
                            valid = false,
                            messages = listOf("Received empty json")
                        )
                    }
                }
            )
    }

    private fun checkForMultipleNumbers(node: JsonNode): Validated<String, Unit> {
        val unmodified: Set<Int> = node.elements().asSequence()
            .filter { commonParsing.isNonEmptyResult(it) }
            .map { it["m_raceNumber"].asInt() }
            .groupBy { it }
            .filter { it.value.size > 1 }.keys

        return if (unmodified.isEmpty())
            Validated.Valid(Unit)
        else {
            Validated.Invalid("the m_raceNumber(s) $unmodified occurred multiple times")
        }
    }
}
