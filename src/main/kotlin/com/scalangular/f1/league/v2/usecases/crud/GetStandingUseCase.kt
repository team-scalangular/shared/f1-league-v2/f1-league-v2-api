package com.scalangular.f1.league.v2.usecases.crud

import arrow.core.Either
import com.scalangular.f1.league.v2.generated.transfer.entitys.RaceResult
import com.scalangular.f1.league.v2.generated.transfer.entitys.Standing

interface GetStandingUseCase {
    data class GetStandingCommand(
        val raceResults: List<RaceResult>
    )

    sealed class StandingError(val message: String) {
        object NoRaceResults : StandingError("No Raceresults provided")
    }

    suspend fun getStanding(command: GetStandingCommand): Either<StandingError, Standing>
}
