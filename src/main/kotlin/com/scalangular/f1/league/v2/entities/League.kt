package com.scalangular.f1.league.v2.entities

import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class League(
    val leagueName: String,
    @Id @GeneratedValue(strategy = GenerationType.AUTO) val leagueId: UUID?
)
