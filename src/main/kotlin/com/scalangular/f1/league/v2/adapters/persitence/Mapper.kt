package com.scalangular.f1.league.v2.adapters.persitence

import com.scalangular.f1.league.v2.generated.transfer.entitys.Race

object Mapper {
    fun map(races: List<com.scalangular.f1.league.v2.entities.Race>) = races.map {
        Race(
            circuit = it.circuit,
            raceId = it.raceId,
            racePositionIndex = it.racePositionIndex,
            date = it.raceDate?.toLocalDate()
        )
    }

}