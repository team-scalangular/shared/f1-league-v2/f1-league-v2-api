package com.scalangular.f1.league.v2.entities

import java.util.*
import javax.persistence.*

@Entity
data class F1Driver(
    val name: String,
    val surename: String?,
    val carNumber: Int?,
    @Id @GeneratedValue(strategy = GenerationType.AUTO) val driverId: UUID?,
    @ManyToOne val league: League
)
