package com.scalangular.f1.league.v2.usecases.crud

import arrow.core.*
import com.scalangular.f1.league.v2.entities.League
import com.scalangular.f1.league.v2.ports.output.GetLeaguePort

class DefaultGetLeagueUseCase(private val getLeaguePort: GetLeaguePort) : GetLeagueUseCase {
    override fun getLeague(command: GetLeagueUseCase.GetLeagueCommand): Either<GetLeagueUseCase.GetLeagueError, League> {
        val league: Option<League> = getLeaguePort.getLeague(
            GetLeaguePort.GetLeagueCommand(
                leagueId = command.leagueId
            )
        )

        return when (league) {
            is Some -> league.value.right()
            is None -> GetLeagueUseCase.GetLeagueError.LeagueNotFound.left()
        }
    }
}
