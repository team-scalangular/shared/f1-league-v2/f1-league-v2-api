package com.scalangular.f1.league.v2

import com.scalangular.f1.league.v2.adapters.CorsFilter
import com.scalangular.f1.league.v2.adapters.persitence.PersistenceAdapter
import com.scalangular.f1.league.v2.adapters.web.AliveHandler
import com.scalangular.f1.league.v2.adapters.web.ExportHandler
import com.scalangular.f1.league.v2.adapters.web.LeagueHandler
import com.scalangular.f1.league.v2.adapters.web.LeaguesHandler
import com.scalangular.f1.league.v2.adapters.web.auth.AuthAdapter
import com.scalangular.f1.league.v2.adapters.web.auth.DefaultAuthWebClient
import com.scalangular.f1.league.v2.adapters.web.auth.ScalAngularAuthAdapter
import com.scalangular.f1.league.v2.ports.input.DefaultSaveRaceResultPort
import com.scalangular.f1.league.v2.ports.output.DefaultLeagueExistsPort
import com.scalangular.f1.league.v2.usecases.common.DefaultAutogenerateDNSForRaceResultUseCase
import com.scalangular.f1.league.v2.usecases.common.DefaultBuildRaceResultsUseCase
import com.scalangular.f1.league.v2.usecases.common.DefaultGetALeaguesNextRaceUseCase
import com.scalangular.f1.league.v2.usecases.crud.*
import com.scalangular.f1.league.v2.usecases.export.MemoizedCreatePdfUseCase
import com.scalangular.f1.league.v2.usecases.parsing.DefaultCommonParsing
import com.scalangular.f1.league.v2.usecases.parsing.DefaultValidateAndPlausibilityUseCase
import com.scalangular.f1.league.v2.usecases.parsing.F12020ParseRaceResultExportUseCase
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.support.BeanDefinitionDsl
import org.springframework.context.support.beans
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.server.awaitBody
import org.springframework.web.reactive.function.server.coRouter
import org.springframework.web.reactive.function.server.router
import java.util.*

fun main(args: Array<String>) {
    runApplication<F1LeagueApiV2>(*args) {
        addInitializers(beans())
    }
}

@SpringBootApplication
@EnableCaching
@EnableScheduling
class F1LeagueApiV2

fun beans(): BeanDefinitionDsl = beans {

    fun String.toUUID(): UUID = UUID.fromString(this)

    bean<AliveHandler>()
    bean<LeaguesHandler>()
    bean<LeagueHandler>()
    bean<DefaultLeagueExistsPort>()
    bean<DefaultSaveRaceResultPort>()
    bean<DefaultGetALeaguesNextRaceUseCase>()
    bean<DefaultGetRaceResultsUseCase>()
    bean<DefaultGetRacesUseCase>()
    bean<DefaultGetLeaguesUseCase>()
    bean<DefaultGetLeagueUseCase>()
    bean<MemoizedF1StandingUseCase>()
    bean<DefaultCommonParsing>()
    bean<CacheDefaultTTL>()
    bean<F12020ParseRaceResultExportUseCase>()
    bean<DefaultValidateAndPlausibilityUseCase>()
    bean<PersistenceAdapter>()
    bean<DefaultAutogenerateDNSForRaceResultUseCase>()
    bean<DefaultBuildRaceResultsUseCase>()
    bean<CorsFilter>()
    bean<ExportHandler>()
    bean<MemoizedCreatePdfUseCase>()
    bean {
        WebClient.builder().build()
    }
    bean<DefaultAuthWebClient>()
    bean<ScalAngularAuthAdapter>()

    bean {
        router {
            val aliveHandler = ref<AliveHandler>()
            val leaguesHandler = ref<LeaguesHandler>()
            val leagueHandler = ref<LeagueHandler>()

            GET("/alive") {
                aliveHandler.isAlive()
            }

            GET("/leagues") {
                leaguesHandler.getLeagues()
            }

            GET("/league/{leagueId}/drivers") {
                leagueHandler.leagueLeagueIdDriversGet(it.pathVariable("leagueId").toUUID())
            }

            GET("/league/{leagueId}") {
                leagueHandler.leagueLeagueIdGet(it.pathVariable("leagueId").toUUID())
            }

            GET("/league/{leagueId}/races") {
                leagueHandler.leagueLeagueIdRacesGet(it.pathVariable("leagueId").toUUID())
            }
        }
    }

    bean {
        coRouter {
            val leagueHandler = ref<LeagueHandler>()
            val exportHandler = ref<ExportHandler>()
            val authAdapter = ref<AuthAdapter>()

            GET("/league/{leagueId}/standing/{raceId}") {
                leagueHandler.leagueLeagueIdStandingRaceIdGet(
                    it.pathVariable("leagueId").toUUID(),
                    it.pathVariable("raceId").toUUID()
                )
            }

            GET("/league/{leagueId}/standing") {
                leagueHandler.leagueLeagueIdStandingGet(it.pathVariable("leagueId").toUUID())
            }

            GET("/league/{leagueId}/raceresult/{raceId}/race/export") {
                exportHandler.exportRaceResult(
                    it.pathVariable("leagueId").toUUID(),
                    it.pathVariable("raceId").toUUID()
                )
            }

            POST("/league/{leagueId}/raceresult/{raceId}/race") {
                authAdapter.authorizeRequest(it, "F1League", 3) {
                    leagueHandler.leagueLeagueIdRaceresultsRaceIdRacePost(
                        it.pathVariable("leagueId").toUUID(),
                        it.pathVariable("raceId").toUUID(),
                        it.awaitBody()
                    )
                }
            }

            GET("/league/{leagueId}/raceresult/{driverId}/driver") {
                leagueHandler.leagueLeagueIdRaceresultsDriverIdDriverGet(
                    it.pathVariable("leagueId").toUUID(),
                    it.pathVariable("driverId").toUUID()
                )
            }

            GET("/league/{leagueId}/raceresults") {
                leagueHandler.leagueLeagueIdRaceresultsGet(it.pathVariable("leagueId").toUUID())
            }

            GET("/league/{leagueId}/raceresult/{raceId}/race") {
                leagueHandler.leagueLeagueIdRaceresultsRaceIdRaceGet(
                    it.pathVariable("leagueId").toUUID(),
                    it.pathVariable("raceId").toUUID()
                )
            }
        }
    }
}
