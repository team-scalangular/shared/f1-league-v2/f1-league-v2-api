package com.scalangular.f1.league.v2.usecases.crud

import com.scalangular.f1.league.v2.entities.League
import com.scalangular.f1.league.v2.ports.output.GetLeaguesPort

class DefaultGetLeaguesUseCase(private val getLeaguesPort: GetLeaguesPort) : GetLeaguesUseCase {
    override fun getLeagues(): List<League> {
        return getLeaguesPort.getLeagues()
    }
}
