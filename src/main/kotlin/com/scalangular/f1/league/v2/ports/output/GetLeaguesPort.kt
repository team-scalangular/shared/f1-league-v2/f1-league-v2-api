package com.scalangular.f1.league.v2.ports.output

import com.scalangular.f1.league.v2.entities.League

interface GetLeaguesPort {
    fun getLeagues(): List<League>
}
