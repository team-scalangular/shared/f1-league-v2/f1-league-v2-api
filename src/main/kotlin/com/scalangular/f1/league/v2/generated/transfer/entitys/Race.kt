package com.scalangular.f1.league.v2.generated.transfer.entitys

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull

/**
 *
 * @param circuit
 * @param raceId
 * @param racePositionIndex
 * @param date
 */
data class Race(

    @get:NotNull
    @JsonProperty("circuit") val circuit: F1Circuit,

    @JsonProperty("raceId") val raceId: java.util.UUID? = null,

    @JsonProperty("racePositionIndex") val racePositionIndex: kotlin.Int? = null,

    @JsonProperty("date") val date: java.time.LocalDate? = null
)
