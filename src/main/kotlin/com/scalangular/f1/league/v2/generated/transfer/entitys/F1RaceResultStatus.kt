package com.scalangular.f1.league.v2.generated.transfer.entitys

/**
 * Result status (invalid, inactive, active, finished, disqualified, not classified,  6 = retired
 * Values: invalid,inactive,active,finished,disqualified,notClassified,retired
 */
enum class F1RaceResultStatus(val value: String) {

    invalid("invalid"), // 0

    inactive("inactive"), // 1

    active("active"), // 2

    finished("finished"), // 3

    disqualified("disqualified"), // 4

    notClassified("notClassified"), // 5

    retired("retired"); // 6
}
