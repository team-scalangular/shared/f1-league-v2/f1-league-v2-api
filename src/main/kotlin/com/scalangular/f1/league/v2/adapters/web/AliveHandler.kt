package com.scalangular.f1.league.v2.adapters.web

import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

class AliveHandler {
    fun isAlive(): Mono<ServerResponse> {
        return ServerResponse.ok().syncBody("Iam Ready to serve")
    }
}
