package com.scalangular.f1.league.v2.repositories

import com.scalangular.f1.league.v2.entities.League
import org.springframework.data.repository.CrudRepository
import java.util.*

interface LeagueRepository : CrudRepository<League, UUID> {
    fun findLeagueByLeagueId(leagueId: UUID): League?
}
