package com.scalangular.f1.league.v2

import io.micrometer.core.instrument.Counter
import io.micrometer.core.instrument.MeterRegistry
import org.springframework.stereotype.Component

@Component
class MetricsLogger(registry: MeterRegistry) {
    private val getLeagueCounter: Counter = Counter
        .builder("get_league_requests")
        .tag("app", "f1-api")
        .register(registry)

    fun getLeagueRequestReceived() = getLeagueCounter.increment()

    private val getStandingCounter: Counter = Counter
        .builder("get_standing_requests")
        .tag("app", "f1-api")
        .register(registry)

    fun getStandingRequestReceived() = getStandingCounter.increment()

    private val getExportCounter: Counter = Counter
        .builder("get_export_requests")
        .tag("app", "f1-api")
        .register(registry)

    fun getExportRequestReceived() = getExportCounter.increment()
}