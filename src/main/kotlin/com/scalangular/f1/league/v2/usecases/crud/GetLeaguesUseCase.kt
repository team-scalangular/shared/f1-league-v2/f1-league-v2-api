package com.scalangular.f1.league.v2.usecases.crud

import com.scalangular.f1.league.v2.entities.League

interface GetLeaguesUseCase {
    fun getLeagues(): List<League>
}
