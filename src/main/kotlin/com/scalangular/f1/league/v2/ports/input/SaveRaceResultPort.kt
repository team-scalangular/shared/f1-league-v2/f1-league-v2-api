package com.scalangular.f1.league.v2.ports.input

import arrow.core.Either
import java.util.*

interface SaveRaceResultPort {
    fun saveRaceResults(
        raceId: UUID,
        results: List<com.scalangular.f1.league.v2.generated.transfer.entitys.DriverRaceResult>
    ): Either<SaveRaceResultError, Unit>

    sealed class SaveRaceResultError {
        data class AlreadyExistingDriverResult(val description: String) : SaveRaceResultError()
        data class RaceNotFound(val raceId: UUID) : SaveRaceResultError()
        data class UnknownDriverEncountered(val driverIds: List<UUID?>) : SaveRaceResultError()
    }
}
