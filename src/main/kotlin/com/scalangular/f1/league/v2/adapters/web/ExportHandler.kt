package com.scalangular.f1.league.v2.adapters.web

import arrow.core.computations.either
import arrow.core.merge
import arrow.fx.coroutines.parZip
import com.scalangular.f1.league.v2.MetricsLogger
import com.scalangular.f1.league.v2.ports.output.LoggerDelegate
import com.scalangular.f1.league.v2.repositories.RaceRepository
import com.scalangular.f1.league.v2.usecases.crud.GetDriversUseCase
import com.scalangular.f1.league.v2.usecases.crud.GetRaceResultsUseCase
import com.scalangular.f1.league.v2.usecases.crud.GetRaceResultsUseCase.GetRaceResultsCommand.GetRaceResultsForRaceCommand
import com.scalangular.f1.league.v2.usecases.export.CreatePdfUseCase
import com.scalangular.f1.league.v2.usecases.export.CreatePdfUseCase.CreatePdfCommand
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.flowOf
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyAndAwait
import org.springframework.web.reactive.function.server.buildAndAwait
import java.util.*

class ExportHandler(
    private val createPdfUseCase: CreatePdfUseCase,
    private val getRaceResultsUseCase: GetRaceResultsUseCase,
    private val getDriversUseCase: GetDriversUseCase,
    private val raceRepository: RaceRepository,
    private val metricsLogger: MetricsLogger
) {
    private val logger by LoggerDelegate()

    @OptIn(FlowPreview::class)
    suspend fun exportRaceResult(leagueId: UUID, raceId: UUID): ServerResponse {
        logger.info("exportRaceResult for league $leagueId and race $raceId")
        metricsLogger.getExportRequestReceived()
        return either.invoke<ServerResponse, ServerResponse> {
            logger.debug("inside invoke")

            val (drivers, raceResults, race) = parZip(
                Dispatchers.IO,
                {
                    logger.debug("gathering driver")
                    getDriversUseCase.getDrivers(GetDriversUseCase.GetDriversCommand(leagueId))
                        .mapLeft { ServerResponse.notFound().buildAndAwait() }.bind()
                },
                {
                    logger.debug("gathering raceResults")
                    getRaceResultsUseCase.getRaceResults(GetRaceResultsForRaceCommand(leagueId, raceId))
                        .mapLeft { ServerResponse.notFound().buildAndAwait() }.bind()
                },
                {
                    logger.debug("gathering race")
                    raceRepository.getRaceByRaceId(raceId)!!
                }
            ) { drivers, raceResults, race -> Triple(drivers, raceResults, race) }

            createPdfUseCase.createPdf(CreatePdfCommand(raceResults.first(), race, drivers))
                .fold(
                    { ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).buildAndAwait() },
                    { pdf ->
                        ServerResponse.ok().contentType(MediaType.APPLICATION_PDF)
                            .bodyAndAwait(flowOf(pdf.binary))
                    }
                )
        }.merge()
    }
}
