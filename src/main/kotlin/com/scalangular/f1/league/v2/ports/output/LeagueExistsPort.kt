package com.scalangular.f1.league.v2.ports.output

import java.util.*

interface LeagueExistsPort {
    fun leagueExists(leagueId: UUID): Boolean
}
