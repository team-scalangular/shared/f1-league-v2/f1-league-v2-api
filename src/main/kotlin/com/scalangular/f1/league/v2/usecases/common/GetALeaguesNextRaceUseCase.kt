package com.scalangular.f1.league.v2.usecases.common

import arrow.core.Either
import com.scalangular.f1.league.v2.generated.transfer.entitys.Race
import java.util.*

interface GetALeaguesNextRaceUseCase {
    data class GetALeaguesNextRaceCommand(
        val leagueId: UUID
    )

    sealed class GetALeaguesNextRaceError {
        object LeagueNotFound : GetALeaguesNextRaceError()
        object NoNextRace : GetALeaguesNextRaceError()
    }

    fun getNextRaceForLeague(command: GetALeaguesNextRaceCommand): Either<GetALeaguesNextRaceError, Race>
}
