package com.scalangular.f1.league.v2.repositories

import com.scalangular.f1.league.v2.entities.F1Driver
import org.springframework.cache.annotation.Cacheable
import org.springframework.data.repository.CrudRepository
import java.util.*

interface DriverRepository : CrudRepository<F1Driver, UUID> {
    @Cacheable(value = ["drivers"], unless = "#a0=='Foundation'")
    fun getAllByLeagueLeagueId(leagueId: UUID): List<F1Driver>

    fun existsByDriverId(driverId: UUID): Boolean

    @Cacheable(value = ["drivers"], unless = "#a0=='Foundation'")
    fun getDriverByDriverId(driverId: UUID): F1Driver?
}
