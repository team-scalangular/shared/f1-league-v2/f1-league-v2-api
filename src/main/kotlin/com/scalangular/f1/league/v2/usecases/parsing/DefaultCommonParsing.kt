package com.scalangular.f1.league.v2.usecases.parsing

import com.fasterxml.jackson.databind.JsonNode
import com.scalangular.f1.league.v2.usecases.CommonParsing

class DefaultCommonParsing : CommonParsing {

    val fieldsToCheck = listOf(
        "m_position",
        "m_numLaps",
        "m_gridPosition",
        "m_points",
        "m_numPitStops",
        "m_resultStatus",
        "m_bestLapTime",
        "m_totalRaceTime",
        "m_penaltiesTime",
        "m_numPenalties",
        "m_numTyreStints"
    )

    override fun isNonEmptyResult(node: JsonNode): Boolean {
        return fieldsToCheck.map {
            isEmpty(node, it)
        }.any { !it }
    }

    private fun isEmpty(node: JsonNode, key: String): Boolean {
        return (node[key]?.asInt(0) ?: 0) == 0
    }
}
