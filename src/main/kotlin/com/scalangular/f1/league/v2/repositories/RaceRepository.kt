package com.scalangular.f1.league.v2.repositories

import com.scalangular.f1.league.v2.entities.Race
import org.springframework.cache.annotation.Cacheable
import org.springframework.data.repository.CrudRepository
import java.util.*

interface RaceRepository : CrudRepository<Race, UUID> {
    @Cacheable(value = ["races"], unless = "#a0=='Foundation'")
    fun getRacesByLeagueLeagueId(leagueId: UUID): List<Race>

    @Cacheable(value = ["races"], unless = "#a0=='Foundation'")
    fun getRaceByRaceId(raceId: UUID): Race?
}
