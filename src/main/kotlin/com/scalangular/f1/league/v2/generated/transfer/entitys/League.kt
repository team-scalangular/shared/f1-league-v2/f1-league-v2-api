package com.scalangular.f1.league.v2.generated.transfer.entitys

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull

/**
 *
 * @param name
 * @param leagueId
 * @param listOfDriverIds
 * @param listOfRaceIds
 */
data class League(
    @get:NotNull
    @JsonProperty("name") val name: kotlin.String,

    @JsonProperty("leagueId") val leagueId: java.util.UUID? = null,

    @JsonProperty("drivers") val drivers: List<F1Driver>,

    @JsonProperty("nextCircuit") val nextCircuit: F1Circuit? = null
)
