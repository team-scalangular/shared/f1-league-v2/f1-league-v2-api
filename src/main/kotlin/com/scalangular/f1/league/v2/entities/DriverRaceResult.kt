package com.scalangular.f1.league.v2.entities

import com.scalangular.f1.league.v2.generated.transfer.entitys.F1RaceResultStatus
import java.util.*
import javax.persistence.*

@Entity
data class DriverRaceResult(
    @Id @GeneratedValue(strategy = GenerationType.AUTO) val driverRaceResultId: UUID?,
    @ManyToOne val race: Race,
    @ManyToOne val f1Driver: F1Driver,
    val endPosition: Int,
    val drivenLaps: Int,
    val gridPosition: Int,
    val bestLapTime: Float,
    val totalRaceTime: Double,
    val penaltiesAmount: Int,
    val penaltiesTime: Float,
    val pitStops: Int,
    val raceNumber: Int,
    val raceResultStatus: F1RaceResultStatus
)
