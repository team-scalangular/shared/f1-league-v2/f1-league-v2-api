package com.scalangular.f1.league.v2.adapters.web.auth

import org.springframework.web.reactive.function.client.ClientResponse

interface AuthWebClint {
    suspend fun authorize(url: String, token: String, resource: String, level: Int): ClientResponse
}
