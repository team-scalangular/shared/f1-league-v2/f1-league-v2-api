package com.scalangular.f1.league.v2.ports.input

import arrow.core.Either
import arrow.core.flatMap
import arrow.core.left
import arrow.core.right
import com.scalangular.f1.league.v2.entities.Race
import com.scalangular.f1.league.v2.generated.transfer.entitys.DriverRaceResult
import com.scalangular.f1.league.v2.ports.input.SaveRaceResultPort.SaveRaceResultError
import com.scalangular.f1.league.v2.ports.output.LoggerDelegate
import com.scalangular.f1.league.v2.repositories.DriverRaceResultRepository
import com.scalangular.f1.league.v2.repositories.DriverRepository
import com.scalangular.f1.league.v2.repositories.RaceRepository
import org.springframework.cache.CacheManager
import java.util.*

class DefaultSaveRaceResultPort(
    private val driverRaceResultRepository: DriverRaceResultRepository,
    private val raceRepository: RaceRepository,
    private val driverRepository: DriverRepository,
    private val cacheManager: CacheManager
) : SaveRaceResultPort {

    private val logger by LoggerDelegate()

    override fun saveRaceResults(raceId: UUID, results: List<DriverRaceResult>): Either<SaveRaceResultError, Unit> {
        logger.info("Saving results for$raceId")
        val race = raceRepository.getRaceByRaceId(raceId)

        return saveResultsWhenRaceExists(race, results, raceId).map {
            logger.debug("clearing cache for driverRaceResults")
            cacheManager.getCache("driverRaceResults")?.clear()
        }
    }

    private fun saveResultsWhenRaceExists(
        race: Race?,
        results: List<DriverRaceResult>,
        raceId: UUID
    ) = if (race != null) {
        hasExistingDriverResults(race).flatMap { _ ->
            hasUnknownDrivers(results).flatMap { _ ->
                results.stream()
                    .map {
                        val driver = driverRepository.getDriverByDriverId(it.driverId!!)
                        com.scalangular.f1.league.v2.entities.DriverRaceResult(
                            race = race,
                            endPosition = it.endPosition,
                            pitStops = it.pitStops,
                            penaltiesTime = it.penaltiesTime,
                            penaltiesAmount = it.penaltiesAmount,
                            totalRaceTime = it.totalRaceTime,
                            bestLapTime = it.bestLapTime,
                            gridPosition = it.gridPosition,
                            drivenLaps = it.drivenLaps,
                            f1Driver = driver!!,
                            driverRaceResultId = null,
                            raceNumber = it.raceNumber,
                            raceResultStatus = it.raceResultStatus
                        )
                    }.forEach {
//                        driverRaceResultRepository.save(it)
                    }

                logger.info("Saved results successfully")
                Unit.right()
            }
        }
    } else {
        SaveRaceResultError.RaceNotFound(raceId).left()
    }

    private fun hasUnknownDrivers(results: List<DriverRaceResult>): Either<SaveRaceResultError, Unit> {
        val unknownDriverResults = results.filter {
            if (it.driverId == null) false
            else !driverRepository.existsByDriverId(it.driverId)
        }

        return Either.conditionally(
            unknownDriverResults.isEmpty(),
            {
                val unknownDriverIds = unknownDriverResults.map { it.driverId }
                logger.warn("Cannot save results because: the driverIds $unknownDriverIds have been encountered")
                SaveRaceResultError.UnknownDriverEncountered(unknownDriverIds)
            },
            {
            }
        )
    }

    private fun hasExistingDriverResults(race: Race): Either<SaveRaceResultError, Unit> {
        val results = driverRaceResultRepository.getAllByRaceRaceId(race.raceId!!)
        return if (results.isNotEmpty()) {
            val message =
                "For drivers ${results.map { it.f1Driver.driverId }} there is already a result for race ${race.raceId}"
            logger.warn("Cannot save results because: $message")
            SaveRaceResultError.AlreadyExistingDriverResult(message).left()
        } else {
            Unit.right()
        }
    }
}
