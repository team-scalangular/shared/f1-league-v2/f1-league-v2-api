package com.scalangular.f1.league.v2.usecases.crud

import arrow.core.Either
import arrow.core.Option
import com.scalangular.f1.league.v2.generated.transfer.entitys.RaceResult
import java.util.*

interface GetRaceResultsUseCase {

    sealed class GetRaceResultsCommand {
        data class GetRaceResultsForDriverCommand(
            val leagueId: UUID,
            val driverId: UUID
        ) : GetRaceResultsCommand()

        data class GetRaceResultsForRaceCommand(
            val leagueId: UUID,
            val raceId: UUID
        ) : GetRaceResultsCommand()

        data class GetRaceResultsForLeagueCommand(
            val leagueId: UUID,
            val racePositionIndex: Option<UUID>
        ) : GetRaceResultsCommand()
    }

    sealed class GetRaceResultsError {
        object DriverNotFoundError : GetRaceResultsError()
        object LeagueNotFoundError : GetRaceResultsError()
        object RaceNotFoundError : GetRaceResultsError()
    }

    suspend fun getRaceResults(command: GetRaceResultsCommand): Either<GetRaceResultsError, List<RaceResult>>
}
