package com.scalangular.f1.league.v2.usecases

import com.fasterxml.jackson.databind.JsonNode

interface CommonParsing {
    fun isNonEmptyResult(node: JsonNode): Boolean
}
