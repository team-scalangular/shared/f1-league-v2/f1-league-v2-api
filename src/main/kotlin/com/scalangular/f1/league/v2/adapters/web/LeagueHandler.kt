package com.scalangular.f1.league.v2.adapters.web

import arrow.core.*
import arrow.core.computations.either
import com.scalangular.f1.league.v2.MetricsLogger
import com.scalangular.f1.league.v2.generated.transfer.entitys.*
import com.scalangular.f1.league.v2.ports.input.SaveRaceResultPort
import com.scalangular.f1.league.v2.ports.output.LoggerDelegate
import com.scalangular.f1.league.v2.usecases.common.GetALeaguesNextRaceUseCase
import com.scalangular.f1.league.v2.usecases.common.GetALeaguesNextRaceUseCase.GetALeaguesNextRaceCommand
import com.scalangular.f1.league.v2.usecases.crud.*
import com.scalangular.f1.league.v2.usecases.crud.GetDriversUseCase.GetDriversCommand
import com.scalangular.f1.league.v2.usecases.crud.GetLeagueUseCase.GetLeagueCommand
import com.scalangular.f1.league.v2.usecases.crud.GetRaceResultsUseCase.GetRaceResultsCommand.*
import com.scalangular.f1.league.v2.usecases.crud.GetRaceResultsUseCase.GetRaceResultsError
import com.scalangular.f1.league.v2.usecases.crud.GetStandingUseCase.GetStandingCommand
import com.scalangular.f1.league.v2.usecases.parsing.ParseRaceResultExportUseCase
import com.scalangular.f1.league.v2.usecases.parsing.ParseRaceResultExportUseCase.ParseRaceResultCommand
import com.scalangular.f1.league.v2.usecases.parsing.ParseRaceResultExportUseCase.ParsingError
import com.scalangular.f1.league.v2.usecases.parsing.ValidateAndPlausibilityUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.withContext
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyAndAwait
import org.springframework.web.reactive.function.server.bodyValueAndAwait
import org.springframework.web.reactive.function.server.buildAndAwait
import reactor.core.publisher.Mono
import java.util.*

@OptIn(FlowPreview::class)
class LeagueHandler(
    private val getDriversUseCase: GetDriversUseCase,
    private val getLeagueUseCase: GetLeagueUseCase,
    private val getRaceResultsUseCase: GetRaceResultsUseCase,
    private val parseRaceResultsUseCase: ParseRaceResultExportUseCase,
    private val getALeaguesNextRaceUseCase: GetALeaguesNextRaceUseCase,
    private val saveResultsUseCase: SaveRaceResultPort,
    private val validateAndPlausibilityUseCase: ValidateAndPlausibilityUseCase,
    private val getRacesUseCase: GetRacesUseCase,
    private val getStandingUseCase: GetStandingUseCase,
    private val metricsLogger: MetricsLogger
) {

    private val logger by LoggerDelegate()

    fun leagueLeagueIdDriversGet(leagueId: UUID): Mono<ServerResponse> {
        logger.info("leagueLeagueIdDriversGet request for id $leagueId")

        return getDriversUseCase.getDrivers(GetDriversCommand(leagueId))
            .fold(
                {
                    buildNotFoundResponse("league", leagueId)
                },
                { drivers ->
                    ServerResponse.ok().syncBody(
                        drivers.map {
                            entityDriverToTransferDriver(it)
                        }
                    )
                }
            )
    }

    private fun buildNotFoundResponse(element: String, id: UUID): Mono<ServerResponse> {
        logger.info("$element with $id not found")
        return ServerResponse.notFound().build()
    }

    private suspend fun buildAsyncNotFoundResponse(element: String, id: UUID): ServerResponse {
        logger.info("$element with $id not found")
        return ServerResponse.notFound().buildAndAwait()
    }

    private fun entityDriverToTransferDriver(it: com.scalangular.f1.league.v2.entities.F1Driver) = F1Driver(
        name = it.name,
        driverId = it.driverId,
        surename = it.surename,
        carNumber = it.carNumber,
    )

    fun leagueLeagueIdGet(leagueId: UUID): Mono<ServerResponse> {
        logger.info("leagueLeagueIdGet request for id $leagueId")
        metricsLogger.getLeagueRequestReceived()

        return either.eager<Mono<ServerResponse>, Mono<ServerResponse>> {
            val league = getLeagueUseCase.getLeague(GetLeagueCommand(leagueId))
                .mapLeft { buildNotFoundResponse("league", leagueId) }.bind()
            val drivers = getDriversUseCase.getDrivers(GetDriversCommand(leagueId))
                .mapLeft { buildNotFoundResponse("league", leagueId) }.bind()
            val nextRace =
                getALeaguesNextRaceUseCase.getNextRaceForLeague(GetALeaguesNextRaceCommand(leagueId)).orNull()

            ServerResponse.ok().syncBody(buildTransferLeagueFromLeagueAndDriver(league, drivers, nextRace?.circuit))
        }.merge()
    }

    private fun buildTransferLeagueFromLeagueAndDriver(
        league: com.scalangular.f1.league.v2.entities.League,
        drivers: List<com.scalangular.f1.league.v2.entities.F1Driver>,
        circuit: F1Circuit?
    ) = League(
        name = league.leagueName,
        leagueId = league.leagueId,
        drivers = drivers.map {
            F1Driver(
                surename = it.surename,
                name = it.name,
                carNumber = it.carNumber,
                driverId = it.driverId
            )
        },
        nextCircuit = circuit
    )

    suspend fun leagueLeagueIdRaceresultsDriverIdDriverGet(
        leagueId: UUID,
        driverId: UUID
    ): ServerResponse = coroutineScope {
        logger.info("leagueLeagueIdRacerresultsDriverIdDriverGet request for league $leagueId and driver $driverId")
        getRaceResultsUseCase.getRaceResults(
            GetRaceResultsForDriverCommand(
                leagueId,
                driverId
            )
        ).fold(
            {
                when (it) {
                    GetRaceResultsError.DriverNotFoundError -> buildAsyncNotFoundResponse(
                        "driver",
                        driverId
                    )
                    GetRaceResultsError.LeagueNotFoundError -> buildAsyncNotFoundResponse(
                        "league",
                        leagueId
                    )
                    GetRaceResultsError.RaceNotFoundError -> {
                        logger.error("getRaceResultsUseCase for GetRaceResultsForDriverCommand return RaceNotFound which shouldn't be possible")
                        ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).buildAndAwait()
                    }
                }
            },
            { results: List<RaceResult> ->
                ServerResponse.ok().bodyAndAwait(results.asFlow())
            }
        )
    }

    suspend fun leagueLeagueIdRaceresultsGet(leagueId: UUID): ServerResponse = coroutineScope {
        logger.info("leagueLeagueIdRaceresultsGet request for league $leagueId")

        getRaceResultsUseCase.getRaceResults(GetRaceResultsForLeagueCommand(leagueId, None))
            .fold(
                {
                    when (it) {
                        GetRaceResultsError.DriverNotFoundError -> {
                            logger.error("getRaceResultsUseCase for GetRaceResultsForLeagueCommand return DriverNotFoundError which shouldn't be possible")
                            ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).buildAndAwait()
                        }
                        GetRaceResultsError.LeagueNotFoundError -> buildAsyncNotFoundResponse(
                            "league",
                            leagueId
                        )
                        GetRaceResultsError.RaceNotFoundError -> {
                            logger.error("getRaceResultsUseCase for GetRaceResultsForLeagueCommand return RaceNotFound which shouldn't be possible")
                            ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).buildAndAwait()
                        }
                    }
                },
                { results ->
                    ServerResponse.ok().bodyAndAwait(results.asFlow())
                }
            )
    }

    suspend fun leagueLeagueIdRaceresultsRaceIdRaceGet(leagueId: UUID, raceId: UUID): ServerResponse =
        coroutineScope {
            logger.info("leagueLeagueIdRaceresultsRaceIdRaceGet request for league $leagueId and race $raceId")

            getRaceResultsUseCase.getRaceResults(
                GetRaceResultsForRaceCommand(
                    leagueId,
                    raceId
                )
            ).fold(
                {
                    when (it) {
                        GetRaceResultsError.DriverNotFoundError -> {
                            logger.error("getRaceResultsUseCase for GetRaceResultsForRaceCommand return DriverNotFoundError which shouldn't be possible")
                            ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).buildAndAwait()
                        }
                        GetRaceResultsError.LeagueNotFoundError -> buildAsyncNotFoundResponse(
                            "league",
                            leagueId
                        )
                        GetRaceResultsError.RaceNotFoundError -> buildAsyncNotFoundResponse(
                            "race",
                            raceId
                        )
                    }
                },
                { results: List<RaceResult> ->
                    ServerResponse.ok().bodyValueAndAwait(results.first())
                }
            )
        }

    suspend fun leagueLeagueIdRaceresultsRaceIdRacePost(
        leagueId: UUID,
        raceId: UUID,
        raceExport: String?
    ): ServerResponse {
        logger.info("leagueLeagueIdRaceresultsRaceIdRacePost request for league $leagueId and race $raceId")

        logger.debug("raceExport:/n${raceExport ?: ""}")

        return if (raceExport == null) {
            logger.info("received null for export")
            ServerResponse.status(HttpStatus.BAD_REQUEST).buildAndAwait()
        } else {
            parseAndValidate(raceExport, leagueId, raceId)
        }
    }

    private suspend fun parseAndValidate(
        raceExport: String,
        leagueId: UUID,
        raceId: UUID
    ): ServerResponse {
        logger.info("validating export")
        val validateResult: ValidateAndPlausibilityUseCase.ValidationAndPlausibilityResult =
            validateAndPlausibilityUseCase.validateAndPlausibilityCheckExport(
                ValidateAndPlausibilityUseCase.ValidateUseCaseCommand(
                    raceExport
                )
            )

        return if (!validateResult.valid) {
            logger.info("export was invalid\nreason: ${validateResult.messages}")
            ServerResponse.badRequest().bodyValueAndAwait(validateResult.messages)
        } else {
            logger.info("export is valid")
            parseExport(leagueId, raceId, raceExport)
        }
    }

    private suspend fun parseExport(
        leagueId: UUID,
        raceId: UUID,
        raceExport: String
    ): ServerResponse {
        logger.info("parsing export")
        return when (
            val drivers =
                getDriversUseCase.getDrivers(GetDriversCommand(leagueId))
        ) {
            is Either.Left -> {
                logger.info("No drivers for league $leagueId found")
                ServerResponse.badRequest().bodyValueAndAwait("No drivers for this league found")
            }
            is Either.Right -> {
                val parserResult: Either<ParsingError, RaceResult> = parseRaceResultsUseCase.parseRaceResult(
                    ParseRaceResultCommand(
                        raceId = raceId,
                        export = raceExport,
                        f1Drivers = drivers.value
                    )
                )

                when (parserResult) {
                    is Either.Left -> {
                        logger.info("Parsing the export isn't possible\nreason:${parserResult.value.message}")
                        ServerResponse.badRequest().bodyValueAndAwait(parserResult.value.message)
                    }
                    is Either.Right -> {
                        val result: List<DriverRaceResult>? = parserResult.value.driverResults

                        if (result == null) {
                            logger.error("Parsed export successfully but DriverRaceResult was null")
                            ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).buildAndAwait()
                        } else {
                            logger.info("Parsed export successfully")
                            saveResultsUseCase.saveRaceResults(raceId, result).fold(
                                { error -> handleSaveResultsError(error) },
                                { ServerResponse.ok().bodyValueAndAwait(result) }
                            )
                        }
                    }
                }
            }
        }
    }

    private suspend fun handleSaveResultsError(error: SaveRaceResultPort.SaveRaceResultError) =
        when (error) {
            is SaveRaceResultPort.SaveRaceResultError.AlreadyExistingDriverResult ->
                ServerResponse.badRequest().bodyValueAndAwait(error.description)
            is SaveRaceResultPort.SaveRaceResultError.RaceNotFound ->
                ServerResponse.status(HttpStatus.NOT_FOUND)
                    .bodyValueAndAwait("Race with ${error.raceId} not found")
            is SaveRaceResultPort.SaveRaceResultError.UnknownDriverEncountered ->
                ServerResponse.badRequest()
                    .bodyValueAndAwait("Cannot save drivers because for ${error.driverIds} there are existing results")
        }

    fun leagueLeagueIdRacesGet(leagueId: UUID): Mono<ServerResponse> {
        logger.info("leagueLeagueIdRacesGet request for league $leagueId")
        return getRacesUseCase.getRaces(GetRacesUseCase.GetRacesCommand(leagueId)).fold(
            {
                buildNotFoundResponse("no races for league", leagueId)
            },
            { races: List<Race> ->
                ServerResponse.ok().syncBody(races)
            }
        )
    }

    suspend fun leagueLeagueIdStandingGet(leagueId: UUID): ServerResponse {
        logger.info("leagueLeagueIdStandingGet request for league $leagueId")
        metricsLogger.getStandingRequestReceived()
        logger.debug("getting results")
        val raceResults = withContext(Dispatchers.IO) {
            logger.debug("starting blocking call")

            val results = getRaceResultsUseCase.getRaceResults(
                GetRaceResultsForLeagueCommand(
                    leagueId,
                    None
                )
            )

            logger.debug("after blocking call inside async")
            results
        }
        logger.debug("after blocking call out of async")

        return evaluateStandingWithErrorChecking(raceResults)
    }

    suspend fun leagueLeagueIdStandingRaceIdGet(leagueId: UUID, raceId: UUID): ServerResponse {
        logger.info("leagueLeagueIdStandingRaceIdGet request for league $leagueId and race $raceId")

        val raceResults = withContext(Dispatchers.IO) {
            getRaceResultsUseCase.getRaceResults(
                GetRaceResultsForLeagueCommand(
                    leagueId,
                    raceId.some()
                )
            )
        }

        return evaluateStandingWithErrorChecking(raceResults)
    }

    private suspend fun evaluateStandingWithErrorChecking(raceResults: Either<GetRaceResultsError, List<RaceResult>>): ServerResponse {
        return raceResults.flatMap { results ->
            getStandingUseCase.getStanding(GetStandingCommand(results))
        }.fold(
            {
                logger.info("Couldn't create standing.\nError: $it")
                ServerResponse.notFound().buildAndAwait()
            },
            { standing ->
                ServerResponse.ok().bodyValueAndAwait(standing)
            }
        )
    }
}
