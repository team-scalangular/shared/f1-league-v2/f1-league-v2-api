package com.scalangular.f1.league.v2.usecases.crud

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import com.scalangular.f1.league.v2.Memoize
import com.scalangular.f1.league.v2.generated.transfer.entitys.*
import com.scalangular.f1.league.v2.ports.output.LoggerDelegate
import com.scalangular.f1.league.v2.usecases.crud.GetStandingUseCase.GetStandingCommand
import com.scalangular.f1.league.v2.usecases.crud.GetStandingUseCase.StandingError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.streams.asStream

class MemoizedF1StandingUseCase(
    private val pointsByPosition: Map<Int, Int> = mapOf(
        1 to 25,
        2 to 18,
        3 to 15,
        4 to 12,
        5 to 10,
        6 to 8,
        7 to 6,
        8 to 4,
        9 to 2,
        10 to 1
    )
) : GetStandingUseCase {

    private val logger by LoggerDelegate()

    private data class PointsResult(
        val points: Int,
        val driverId: UUID
    )

    override suspend fun getStanding(command: GetStandingCommand): Either<StandingError, Standing> =
        withContext(Dispatchers.Default) {
            standingMemoized(command)
        }

    val standingMemoized by Memoize { command: GetStandingCommand ->
        logger.debug("generating standing")
        if (command.raceResults.isEmpty()) {
            StandingError.NoRaceResults.left()
        } else {
            command.raceResults.asSequence()
                .flatMap { raceResultToPointsResult(it) }
                .groupBy { it.driverId }
                .map { (driverId, driverRaceResults) -> combineDriverResults(driverRaceResults, driverId) }
                .sortedWith { driver1, driver2 ->
                    sortDrivers(driver2, driver1, command)
                }
                .mapIndexed { index, intermediateResult ->
                    pointsResultToStandingRow(index, intermediateResult)
                }.right()
        }
    }

    private fun pointsResultToStandingRow(
        index: Int,
        pointsResult: PointsResult
    ) = StandingRow(
        position = index + 1,
        points = pointsResult.points,
        driverId = pointsResult.driverId
    )

    private fun sortDrivers(
        driver2: PointsResult,
        driver1: PointsResult,
        command: GetStandingCommand
    ) = if (driver2.points == driver1.points) {
        val allRaceResult: List<DriverRaceResult> = command.raceResults.flatMap { it.driverResults ?: emptyList() }
        decideByPositions(driver2, driver1, allRaceResult)
    } else {
        driver2.points - driver1.points
    }

    private tailrec fun decideByPositions(
        driver2: PointsResult,
        driver1: PointsResult,
        raceResults: List<DriverRaceResult>,
        positionToCount: Int = 1,
        maxPositionCount: Int = 24
    ): Int {
        return if (positionToCount > maxPositionCount) 0
        else {
            val driver1Count = countPositionsForDriver(raceResults, driver1, positionToCount)
            val driver2Count = countPositionsForDriver(raceResults, driver2, positionToCount)
            when {
                driver1Count > driver2Count -> -1
                driver1Count < driver2Count -> 1
                else -> decideByPositions(driver2, driver1, raceResults, positionToCount + 1)
            }
        }
    }

    private fun countPositionsForDriver(
        raceResults: List<DriverRaceResult>,
        driver1: PointsResult,
        positionToCount: Int
    ) = raceResults.asSequence().asStream()
        .filter { it.driverId == driver1.driverId && it.endPosition == positionToCount }.count()

    private fun combineDriverResults(value: List<PointsResult>, driverId: UUID): PointsResult {
        return value.fold(PointsResult(0, driverId)) { acc, intermediateResult ->
            acc.copy(points = acc.points + intermediateResult.points)
        }
    }

    private fun raceResultToPointsResult(raceResult: RaceResult): Sequence<PointsResult> {
        val sortedResultsByBestLap: List<DriverRaceResult> =
            sortDriverResultsByFastestLapTime(raceResult.driverResults ?: emptyList())
        val resultWithBestLap: DriverRaceResult? = getFastestResultWithFastestLap(sortedResultsByBestLap)
        val onlyOneFastestLapExists = checkIfThereIsASingleFastestLap(resultWithBestLap, sortedResultsByBestLap)

        return raceResult.driverResults?.fold(
            emptyList()
        ) { acc: List<PointsResult>, driverRaceResult: DriverRaceResult ->
            val isBestLap = onlyOneFastestLapExists && hasFastestLap(driverRaceResult, resultWithBestLap)
            acc.plus(
                PointsResult(
                    driverId = driverRaceResult.driverId ?: TODO(),
                    points = pointsForRaceResult(driverRaceResult, isBestLap),
                )
            )
        }?.asSequence() ?: emptySequence()
    }

    private fun getFastestResultWithFastestLap(sortedResultsByBestLap: List<DriverRaceResult>) =
        if (sortedResultsByBestLap.isEmpty()) null else sortedResultsByBestLap.first()

    private fun checkIfThereIsASingleFastestLap(
        resultWithBestLap: DriverRaceResult?,
        sortedResultsByBestLap: List<DriverRaceResult>
    ) =
        bestLapTimeExists(resultWithBestLap) && sortedResultsByBestLap.count { it.bestLapTime <= resultWithBestLap!!.bestLapTime } == 1

    private fun isCountableResult(driverRaceResult: DriverRaceResult): Boolean =
        driverRaceResult.raceResultStatus == F1RaceResultStatus.finished

    private fun pointsForRaceResult(driverRaceResult: DriverRaceResult, hasFastestLap: Boolean): Int {
        return if (isCountableResult(driverRaceResult)) positionToPoints(driverRaceResult.endPosition) + fastestLapPoint(
            hasFastestLap,
            driverRaceResult
        ) else 0
    }

    private fun fastestLapPoint(
        hasFastestLap: Boolean,
        driverRaceResult: DriverRaceResult
    ) = if (hasFastestLap && driverRaceResult.endPosition <= 10) 1 else 0

    private fun sortDriverResultsByFastestLapTime(driverRaceResults: List<DriverRaceResult>) =
        driverRaceResults.filter { isCountableResult(it) }.sortedBy { it.bestLapTime }

    private fun hasFastestLap(
        currentDriverResult: DriverRaceResult,
        bestLapTimeResult: DriverRaceResult?
    ): Boolean =
        isCountableResult(currentDriverResult) && bestLapTimeExists(bestLapTimeResult) && driverHasTheBestLapTime(
            currentDriverResult,
            bestLapTimeResult
        )

    private fun driverHasTheBestLapTime(
        currentDriverResult: DriverRaceResult,
        bestLapTimeResult: DriverRaceResult?
    ) = currentDriverResult.driverId == bestLapTimeResult!!.driverId

    private fun bestLapTimeExists(bestLapTimeResult: DriverRaceResult?) =
        bestLapTimeResult != null

    private fun positionToPoints(position: Int): Int = pointsByPosition.getOrDefault(position, 0)
}
