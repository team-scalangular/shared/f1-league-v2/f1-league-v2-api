package com.scalangular.f1.league.v2.adapters.persitence

import arrow.core.Either
import arrow.core.Option
import arrow.core.left
import arrow.core.right
import com.scalangular.f1.league.v2.entities.F1Driver
import com.scalangular.f1.league.v2.entities.League
import com.scalangular.f1.league.v2.ports.output.GetLeaguePort
import com.scalangular.f1.league.v2.ports.output.GetLeaguePort.GetLeagueCommand
import com.scalangular.f1.league.v2.ports.output.GetLeaguesPort
import com.scalangular.f1.league.v2.ports.output.LeagueExistsPort
import com.scalangular.f1.league.v2.ports.output.LoggerDelegate
import com.scalangular.f1.league.v2.repositories.DriverRepository
import com.scalangular.f1.league.v2.repositories.LeagueRepository
import com.scalangular.f1.league.v2.usecases.crud.GetDriversUseCase
import com.scalangular.f1.league.v2.usecases.crud.GetDriversUseCase.GetDriversCommand
import com.scalangular.f1.league.v2.usecases.crud.GetDriversUseCase.GetDriversError
import com.scalangular.f1.league.v2.usecases.crud.GetDriversUseCase.GetDriversError.LeagueNotFoundError

class PersistenceAdapter(
    private val leagueRepository: LeagueRepository,
    private val driverRepository: DriverRepository,
    private val leagueExistsPort: LeagueExistsPort,
) : GetLeaguePort,
    GetLeaguesPort,
    GetDriversUseCase {

    private val logger by LoggerDelegate()

    override fun getLeague(id: GetLeagueCommand): Option<League> {
        return Option.fromNullable(leagueRepository.findLeagueByLeagueId(id.leagueId))
    }

    override fun getDrivers(command: GetDriversCommand): Either<GetDriversError, List<F1Driver>> {
        logger.debug("getting drivers from db")
        return if (!leagueExistsPort.leagueExists(command.leagueId)) {
            LeagueNotFoundError.left()
        } else {
            driverRepository.getAllByLeagueLeagueId(command.leagueId).right()
        }
    }

    override fun getLeagues(): List<League> {
        return leagueRepository.findAll().toList()
    }
}
