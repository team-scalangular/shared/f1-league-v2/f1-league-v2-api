package com.scalangular.f1.league.v2.usecases.crud

import arrow.core.Either
import com.scalangular.f1.league.v2.entities.League
import java.util.*

interface GetLeagueUseCase {
    data class GetLeagueCommand(
        val leagueId: UUID
    )

    sealed class GetLeagueError {
        object LeagueNotFound : GetLeagueError()
    }

    fun getLeague(command: GetLeagueCommand): Either<GetLeagueError, League>
}
