package com.scalangular.f1.league.v2.generated.transfer.entitys

import com.fasterxml.jackson.annotation.JsonProperty

/**
 *
 * @param leagueId
 * @param name
 * @param nextRace
 */
data class BasicLeague(

    @JsonProperty("leagueId") val leagueId: java.util.UUID? = null,

    @JsonProperty("name") val name: kotlin.String? = null,

    @JsonProperty("nextRace") val nextRace: F1Circuit? = null
)
