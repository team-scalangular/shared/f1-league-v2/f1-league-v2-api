package com.scalangular.f1.league.v2.adapters.web

import arrow.core.Either
import com.scalangular.f1.league.v2.entities.League
import com.scalangular.f1.league.v2.generated.transfer.entitys.BasicLeague
import com.scalangular.f1.league.v2.generated.transfer.entitys.Race
import com.scalangular.f1.league.v2.ports.output.LoggerDelegate
import com.scalangular.f1.league.v2.usecases.common.GetALeaguesNextRaceUseCase
import com.scalangular.f1.league.v2.usecases.common.GetALeaguesNextRaceUseCase.GetALeaguesNextRaceCommand
import com.scalangular.f1.league.v2.usecases.common.GetALeaguesNextRaceUseCase.GetALeaguesNextRaceError
import com.scalangular.f1.league.v2.usecases.crud.GetLeaguesUseCase
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

class LeaguesHandler(
    private val getLeaguesUseCase: GetLeaguesUseCase,
    private val getALeaguesNextRaceUseCase: GetALeaguesNextRaceUseCase
) {
    private val logger by LoggerDelegate()

    fun getLeagues(): Mono<ServerResponse> {
        logger.info("getLeagues request")

        val leagues = getLeaguesUseCase.getLeagues().parallelStream().map { league ->
            val nextRace = getALeaguesNextRaceUseCase.getNextRaceForLeague(
                GetALeaguesNextRaceCommand(
                    league.leagueId!!
                )
            )

            when (nextRace) {
                is Either.Left -> buildLeagueWithoutNextRace(league, nextRace)
                is Either.Right -> buildLeague(league, nextRace)
            }
        }

        return ServerResponse.ok().syncBody(leagues)
    }

    private fun buildLeague(
        league: League,
        nextRace: Either.Right<Race>
    ) = BasicLeague(
        leagueId = league.leagueId,
        name = league.leagueName,
        nextRace = nextRace.value.circuit
    )

    private fun buildLeagueWithoutNextRace(
        league: League,
        nextRace: Either.Left<GetALeaguesNextRaceError>
    ): Any = when (nextRace.value) {
        GetALeaguesNextRaceError.LeagueNotFound ->
            logger.error("getALeaguesNextRaceUseCase return LeagueNotFound for a league which was loaded via the getLeaguesUseCase")
        GetALeaguesNextRaceError.NoNextRace ->
            BasicLeague(
                leagueId = league.leagueId,
                name = league.leagueName,
                nextRace = null
            )
    }
}
