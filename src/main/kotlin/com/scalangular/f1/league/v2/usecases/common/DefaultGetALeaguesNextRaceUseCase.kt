package com.scalangular.f1.league.v2.usecases.common

import arrow.core.Either
import arrow.core.left
import arrow.core.rightIfNotNull
import com.scalangular.f1.league.v2.generated.transfer.entitys.Race
import com.scalangular.f1.league.v2.usecases.common.GetALeaguesNextRaceUseCase.GetALeaguesNextRaceCommand
import com.scalangular.f1.league.v2.usecases.common.GetALeaguesNextRaceUseCase.GetALeaguesNextRaceError
import com.scalangular.f1.league.v2.usecases.common.GetALeaguesNextRaceUseCase.GetALeaguesNextRaceError.LeagueNotFound
import com.scalangular.f1.league.v2.usecases.common.GetALeaguesNextRaceUseCase.GetALeaguesNextRaceError.NoNextRace
import com.scalangular.f1.league.v2.usecases.crud.GetRacesUseCase
import com.scalangular.f1.league.v2.usecases.crud.GetRacesUseCase.GetRacesCommand
import java.time.LocalDate

class DefaultGetALeaguesNextRaceUseCase(private val getRacesUseCase: GetRacesUseCase) : GetALeaguesNextRaceUseCase {
    override fun getNextRaceForLeague(command: GetALeaguesNextRaceCommand): Either<GetALeaguesNextRaceError, Race> {
        return getRacesUseCase.getRaces(GetRacesCommand(command.leagueId)).fold(
            {
                LeagueNotFound.left()
            },
            { races ->
                getNextRaceFromRaces(races)
            }
        )
    }

    private fun getNextRaceFromRaces(races: List<Race>): Either<NoNextRace, Race> =
        races.asSequence().filter { isInTheFuture(it) }
            .sortedWith { race1, race2 ->
                compareForNextRace(race1, race2)
            }.firstOrNull()
            .rightIfNotNull { NoNextRace }

    private fun compareForNextRace(
        race1: Race,
        race2: Race
    ): Int {
        val race1Date = race1.date ?: LocalDate.MIN
        val race2Date = race2.date ?: LocalDate.MIN

        val diff = race1Date.compareTo(race2Date)

        return if (diff == 0) {
            compareRacePositionIndex(race1, race2)
        } else {
            diff
        }
    }

    private fun compareRacePositionIndex(
        race1: Race,
        race2: Race
    ) = (race1.racePositionIndex ?: Int.MIN_VALUE) - (race2.racePositionIndex ?: Int.MIN_VALUE)

    private fun isInTheFuture(it: Race) = !(it.date ?: LocalDate.MIN).isBefore(LocalDate.now())
}
