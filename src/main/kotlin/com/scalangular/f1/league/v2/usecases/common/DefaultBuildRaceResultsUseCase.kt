package com.scalangular.f1.league.v2.usecases.common

import com.scalangular.f1.league.v2.generated.transfer.entitys.RaceResult
import com.scalangular.f1.league.v2.generated.transfer.entitys.toTransferModel

class DefaultBuildRaceResultsUseCase : BuildRaceResultsUseCase {
    override fun buildRaceResult(buildRaceResultCommand: BuildRaceResultsUseCase.BuildRaceResultCommand): RaceResult {
        return RaceResult(
            raceId = buildRaceResultCommand.race.raceId,
            driverResults = buildRaceResultCommand.driverRaceResults
                .asSequence()
                .filter { driverResult -> driverResult.race.raceId == buildRaceResultCommand.race.raceId }
                .map { it.toTransferModel() }
                .toList()
        )
    }
}
