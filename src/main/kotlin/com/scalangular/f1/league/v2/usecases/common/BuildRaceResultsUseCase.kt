package com.scalangular.f1.league.v2.usecases.common

import com.scalangular.f1.league.v2.entities.DriverRaceResult
import com.scalangular.f1.league.v2.entities.Race
import com.scalangular.f1.league.v2.generated.transfer.entitys.RaceResult

interface BuildRaceResultsUseCase {

    data class BuildRaceResultCommand(
        val race: Race,
        val driverRaceResults: List<DriverRaceResult>,
    )

    fun buildRaceResult(buildRaceResultCommand: BuildRaceResultCommand): RaceResult
}
