package com.scalangular.f1.league.v2.ports.output

import com.scalangular.f1.league.v2.repositories.LeagueRepository
import java.util.*

class DefaultLeagueExistsPort(private val leagueRepository: LeagueRepository) : LeagueExistsPort {
    override fun leagueExists(leagueId: UUID): Boolean = leagueRepository.findLeagueByLeagueId(leagueId) != null
}
