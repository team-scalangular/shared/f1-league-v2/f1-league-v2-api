package com.scalangular.f1.league.v2.adapters.web.auth

import com.scalangular.f1.league.v2.ports.output.LoggerDelegate
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitExchange

class DefaultAuthWebClient(private val webClient: WebClient) : AuthWebClint {

    private val logger by LoggerDelegate()

    override suspend fun authorize(url: String, token: String, resource: String, level: Int): ClientResponse =
        try {
            webClient.get()
                .uri { builder ->
                    val uri = builder.path(url)
                        .queryParam("resource", resource)
                        .queryParam("level", level)
                        .build()
                    logger.debug(uri.toString())
                    uri
                }
                .header(HttpHeaders.AUTHORIZATION, token)
                .awaitExchange()
        } catch (e: Exception) {
            logger.error(e.message)
            ClientResponse.create(HttpStatus.INTERNAL_SERVER_ERROR).body("Error while connecting to authapi").build()
        }
}
