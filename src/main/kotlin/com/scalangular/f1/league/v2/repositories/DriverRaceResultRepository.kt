package com.scalangular.f1.league.v2.repositories

import com.scalangular.f1.league.v2.entities.DriverRaceResult
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.Cacheable
import org.springframework.data.repository.CrudRepository
import java.util.*

interface DriverRaceResultRepository : CrudRepository<DriverRaceResult, UUID> {
    @Cacheable(value = ["driverRaceResults"], unless = "#a0=='Foundation'")
    fun getAllByF1DriverDriverId(driverId: UUID): List<DriverRaceResult>

    @Cacheable(value = ["driverRaceResults"], unless = "#a0=='Foundation'")
    fun getAllByRaceRaceId(raceId: UUID): List<DriverRaceResult>

    @CacheEvict(cacheNames = ["driverRaceResults"])
    fun save(driverRaceResult: DriverRaceResult): DriverRaceResult
}
