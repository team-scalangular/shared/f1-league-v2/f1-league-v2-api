package com.scalangular.f1.league.v2

import com.scalangular.f1.league.v2.ports.output.LoggerDelegate
import org.springframework.cache.CacheManager
import org.springframework.scheduling.annotation.Scheduled

class CacheDefaultTTL(
    private val cacheManager: CacheManager
) {

    private val logger by LoggerDelegate()

    @Scheduled(fixedRateString = "\${ttlinterval}")
    fun cacheEvict() {
        logger.debug("scheduled clearing of driverRaceResults")
        cacheManager.getCache("driverRaceResults")?.clear()
        cacheManager.getCache("drivers")?.clear()
        cacheManager.getCache("races")?.clear()
    }
}
