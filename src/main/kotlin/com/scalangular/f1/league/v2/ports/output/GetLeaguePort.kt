package com.scalangular.f1.league.v2.ports.output

import arrow.core.Option
import com.scalangular.f1.league.v2.entities.League
import java.util.*

interface GetLeaguePort {

    data class GetLeagueCommand(
        val leagueId: UUID
    )

    fun getLeague(id: GetLeagueCommand): Option<League>
}
