package com.scalangular.f1.league.v2.usecases.crud

import arrow.core.Either
import com.scalangular.f1.league.v2.entities.F1Driver
import java.util.*

interface GetDriversUseCase {

    data class GetDriversCommand(
        val leagueId: UUID
    )

    sealed class GetDriversError {
        object LeagueNotFoundError : GetDriversError()
    }

    fun getDrivers(command: GetDriversCommand): Either<GetDriversError, List<F1Driver>>
}
