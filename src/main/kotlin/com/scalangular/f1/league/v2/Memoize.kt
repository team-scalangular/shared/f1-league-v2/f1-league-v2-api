package com.scalangular.f1.league.v2

import com.scalangular.f1.league.v2.ports.output.LoggerDelegate
import kotlin.reflect.KProperty

class Memoize<T, R>(val f: (T) -> R) {
    private val logger by LoggerDelegate()

    private val cache = mutableMapOf<T, R>()

    operator fun getValue(thisRef: Any?, property: KProperty<*>) = { n: T ->
        logger.debug("getValue memoized for $f.")
        cache.getOrPut(n) {
            logger.debug("no value for key. invoking $f")
            f(n)
        }
    }
}
