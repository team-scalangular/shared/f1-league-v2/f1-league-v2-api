package com.scalangular.f1.league.v2.usecases.parsing

import arrow.core.*
import arrow.core.computations.either
import arrow.fx.coroutines.parTraverseEither
import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.scalangular.f1.league.v2.generated.transfer.entitys.DriverRaceResult
import com.scalangular.f1.league.v2.generated.transfer.entitys.F1RaceResultStatus
import com.scalangular.f1.league.v2.generated.transfer.entitys.RaceResult
import com.scalangular.f1.league.v2.ports.output.LoggerDelegate
import com.scalangular.f1.league.v2.usecases.CommonParsing
import com.scalangular.f1.league.v2.usecases.parsing.ParseRaceResultExportUseCase.ParseRaceResultCommand
import com.scalangular.f1.league.v2.usecases.parsing.ParseRaceResultExportUseCase.ParsingError
import com.scalangular.f1.league.v2.usecases.parsing.ParseRaceResultExportUseCase.ParsingError.*
import java.util.*
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue

class F12020ParseRaceResultExportUseCase(private val commonParsing: CommonParsing) : ParseRaceResultExportUseCase {

    private val logger by LoggerDelegate()

    @OptIn(ExperimentalTime::class)
    override suspend fun parseRaceResult(command: ParseRaceResultCommand): Either<ParsingError, RaceResult> {
        val result = measureTimedValue {
            if (command.export.trim().isEmpty()) {
                ReceivedEmptyJson.left()
            } else {
                Either.catch {
                    ObjectMapper().readTree(command.export)
                }.fold(
                    { throwable ->
                        when (throwable) {
                            is JsonParseException -> MalformedJsonError(
                                throwable.message ?: "Unknown JsonParseException"
                            ).left()
                            else -> UnknownParsingError(throwable.message ?: "No Message").left()
                        }
                    },
                    { jsonNode: JsonNode -> parseToRaceResults(jsonNode, command) }
                )
            }
        }

        logger.info("Parsing took ${result.duration.inWholeMilliseconds}ms")
        return result.value
    }

    private suspend fun parseToRaceResults(
        jsonNode: JsonNode,
        command: ParseRaceResultCommand
    ): Either<ParsingError, RaceResult> {
        return parseDriverResults(jsonNode, command).map { driverRaceResults ->
            RaceResult(
                command.raceId,
                driverRaceResults.toList()
            )
        }
    }

    private suspend fun parseDriverResults(
        jsonNode: JsonNode,
        command: ParseRaceResultCommand
    ): Either<ParsingError, List<DriverRaceResult>> {
        return if (!jsonNode.isArray) {
            MalformedJsonError("No array found at top level in export").left()
        } else {
            jsonNode.elements().asSequence().toList().filter { commonParsing.isNonEmptyResult(it) }
                .parTraverseEither { driverResultJsonNode: JsonNode ->
                    logger.debug("Parsing jsonNode for m_raceNumber ${driverResultJsonNode["m_raceNumber"]} on ${Thread.currentThread().name}")
                    parseDriverResult(driverResultJsonNode, command)
                }
        }
    }

    private fun parseDriverResult(
        driverResultJsonNode: JsonNode,
        command: ParseRaceResultCommand
    ): Either<ParsingError, DriverRaceResult> =
        either.eager {
            val carNumber = parseCarNumber(driverResultJsonNode).bind()
            val driverId = matchDriver(command, carNumber).bind()
            val endPosition = parseEndPosition(driverResultJsonNode, carNumber).bind()
            val drivenLaps = parseDrivenLaps(driverResultJsonNode, carNumber).bind()
            val gridPosition = parseGridPosition(driverResultJsonNode, carNumber).bind()
            val bestLapTime = parseBestLapTime(driverResultJsonNode, carNumber).bind()
            val totalRaceTime = parseTotalRaceTime(driverResultJsonNode).bind()
            val penaltiesAmount = parsePenaltiesAmount(driverResultJsonNode).bind()
            val penaltiesTime = parsePenaltiesTime(driverResultJsonNode).bind()
            val pitStops = parsePitStops(driverResultJsonNode, carNumber).bind()
            val raceResult = parseRaceResult(driverResultJsonNode, carNumber).bind()

            DriverRaceResult(
                command.raceId,
                endPosition = endPosition,
                drivenLaps = drivenLaps,
                gridPosition = gridPosition,
                bestLapTime = bestLapTime,
                totalRaceTime = totalRaceTime,
                penaltiesAmount = penaltiesAmount,
                penaltiesTime = penaltiesTime,
                pitStops = pitStops,
                driverId = driverId,
                raceNumber = carNumber,
                raceResultStatus = raceResult
            )
        }

    private fun parseRaceResult(
        driverResultJsonNode: JsonNode,
        carNumber: Int
    ) = parseNodeToField(
        driverResultJsonNode,
        parse = {
            parseRawValueWithNodeAndNullCheck(it, "m_resultStatus") { value ->
                Integer.parseInt(value)
            }
        },
        validate = {
            intervalCheck(it, 0, 6) { value ->
                "Field m_resultStatus for driver with raceNumber $carNumber was $value but should be >= 0 and <= 6"
            }
        }
    ).map { matchRaceResult(it) }

    private fun parsePitStops(
        driverResultJsonNode: JsonNode,
        carNumber: Int
    ) = parseNodeToField(
        driverResultJsonNode,
        parse = {
            parseRawValueWithNodeAndNullCheck(it, "m_numPitStops") { value ->
                Integer.parseInt(value)
            }
        },
        validate = { value ->
            intervalCheck(value, 0, 100) {
                "Field m_numPitStops for driver with raceNumber $carNumber was $it but should be >= 0 and <= 100"
            }
        }
    )

    private fun parsePenaltiesTime(driverResultJsonNode: JsonNode) = parseNodeToField(
        driverResultJsonNode,
        parse = { value -> parseRawValueWithNodeAndNullCheck(value, "m_penaltiesTime") { it.toFloat() } }
    )

    private fun parsePenaltiesAmount(driverResultJsonNode: JsonNode) = parseNodeToField(
        driverResultJsonNode,
        parse = {
            parseRawValueWithNodeAndNullCheck(it, "m_numPenalties") { value ->
                Integer.parseInt(value)
            }
        }
    )

    private fun parseTotalRaceTime(driverResultJsonNode: JsonNode) = parseNodeToField(
        driverResultJsonNode,
        parse = { value -> parseRawValueWithNodeAndNullCheck(value, "m_totalRaceTime") { it.toDouble() } }
    )

    private fun parseBestLapTime(driverResultJsonNode: JsonNode, carNumber: Int) = parseNodeToField(
        driverResultJsonNode,
        parse = { value -> parseRawValueWithNodeAndNullCheck(value, "m_bestLapTime") { it.toFloat() } },
        validate = {value ->
            if (value <= 0) IllegalFieldStateError("Field m_bestLapTime is <= $value for m_raceNumber $carNumber").left()
            else value.right()
        }
    )

    private fun parseGridPosition(
        driverResultJsonNode: JsonNode,
        carNumber: Int
    ) = parseNodeToField(
        driverResultJsonNode,
        parse = {
            parseRawValueWithNodeAndNullCheck(it, "m_gridPosition") { value ->
                Integer.parseInt(value)
            }
        },
        validate = { value ->
            intervalCheck(value, 1, 24) {
                "Field m_gridPosition for driver with raceNumber $carNumber was $it but should be >= 1 and <= 24"
            }
        }
    )

    private fun parseDrivenLaps(
        driverResultJsonNode: JsonNode,
        carNumber: Int
    ) = parseNodeToField(
        driverResultJsonNode,
        parse = {
            parseRawValueWithNodeAndNullCheck(it, "m_numLaps") { value ->
                Integer.parseInt(value)
            }
        },
        validate = { value ->
            intervalCheck(value, 0, 100) {
                "Field m_numLaps for driver with raceNumber $carNumber was $it but should be >= 0 and <= 100"
            }
        }
    )

    private fun parseEndPosition(
        driverResultJsonNode: JsonNode,
        carNumber: Int
    ) = parseNodeToField(
        driverResultJsonNode,
        parse = {
            parseRawValueWithNodeAndNullCheck(it, "m_position") { value ->
                Integer.parseInt(value)
            }
        },
        validate = { value ->
            intervalCheck(value, 1, 24) {
                "Field m_position for driver with raceNumber $carNumber was $it but should be >= 1 and <= 24"
            }
        }
    )

    private fun parseCarNumber(driverResultJsonNode: JsonNode) = parseNodeToField(
        driverResultJsonNode,
        {
            parseRawValueWithNodeAndNullCheck(it, "m_raceNumber") { value ->
                Integer.parseInt(value)
            }
        }
    )

    private inline fun <A> parseRawValueWithNodeAndNullCheck(
        driverResultNode: JsonNode,
        field: String,
        mapper: (String) -> A
    ): Either<ParsingError, A> {
        val nodeValue: JsonNode? = driverResultNode[field]

        return when {
            nodeValue == null -> {
                MissingFieldError("Field $field is missing. At $driverResultNode").left()
            }
            nodeValue.isValueNode -> {
                mapper(nodeValue.asText()).right()
            }
            else -> {
                MalformedJsonError("Field $field is a ${nodeValue.nodeType} not a value node. At $driverResultNode").left()
            }
        }
    }

    private inline fun intervalCheck(
        it: Int,
        lower: Int,
        upper: Int,
        message: (Int) -> String
    ): Either<ParsingError, Int> = if (it < lower || it > upper) {
        IllegalFieldStateError(message(it)).left()
    } else {
        it.right()
    }

    private val f1RaceResultByExportValue = mapOf(
        0 to F1RaceResultStatus.invalid,
        1 to F1RaceResultStatus.inactive,
        2 to F1RaceResultStatus.active,
        3 to F1RaceResultStatus.finished,
        4 to F1RaceResultStatus.disqualified,
        5 to F1RaceResultStatus.notClassified,
        6 to F1RaceResultStatus.retired
    )

    private fun matchRaceResult(result: Int): F1RaceResultStatus {
        return f1RaceResultByExportValue[result]!!
    }

    private fun matchDriver(command: ParseRaceResultCommand, carNumber: Int): Either<ParsingError, UUID> {
        val id: UUID? = command.f1Drivers.find { it.carNumber == carNumber }?.driverId
        return id.rightIfNotNull { CantMatchDriverError("carNumber $carNumber cant be matched to a driver") }
    }

    private inline fun <A> parseNodeToField(
        node: JsonNode,
        parse: (JsonNode) -> Either<ParsingError, A>,
        validate: (A) -> Either<ParsingError, A> = { it.right() }
    ): Either<ParsingError, A> {
        return Either.catch {
            parse(node)
        }
            .mapLeft { UnknownParsingError(it.toString()) }
            .flatMap { it.map(validate) }.flatten()
    }
}
