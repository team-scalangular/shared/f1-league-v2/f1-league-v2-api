package com.scalangular.f1.league.v2.generated.transfer.entitys

import com.fasterxml.jackson.annotation.JsonProperty

/**
 *
 * @param name
 * @param driverId
 * @param surename
 * @param carNumber
 */
data class F1Driver(

    @JsonProperty("name") val name: kotlin.String,

    @JsonProperty("driverId") val driverId: java.util.UUID? = null,

    @JsonProperty("surename") val surename: kotlin.String? = null,

    @JsonProperty("carNumber") val carNumber: kotlin.Int? = null
)
