package com.scalangular.f1.league.v2.generated.transfer.entitys

import com.fasterxml.jackson.annotation.JsonProperty

/**
 *
 * @param driverId
 * @param position
 * @param points
 */
data class StandingRow(
    @JsonProperty("driverId") val driverId: java.util.UUID? = null,

    @JsonProperty("position") val position: kotlin.Int? = null,

    @JsonProperty("points") val points: kotlin.Int? = null,
)

typealias Standing = List<StandingRow>
