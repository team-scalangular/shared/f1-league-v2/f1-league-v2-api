package com.scalangular.f1.league.v2.usecases.parsing

import arrow.core.Either
import com.scalangular.f1.league.v2.entities.F1Driver
import com.scalangular.f1.league.v2.generated.transfer.entitys.RaceResult
import java.util.*

interface ParseRaceResultExportUseCase {

    data class ParseRaceResultCommand(
        val raceId: UUID,
        val f1Drivers: List<F1Driver>,
        val export: String
    )

    sealed class ParsingError(open val message: String) {
        object ReceivedEmptyJson : ParsingError("Received Empty Json")
        data class UnknownParsingError(override val message: String) : ParsingError(message)
        data class MalformedJsonError(override val message: String) : ParsingError(message)
        data class CantMatchDriverError(override val message: String) : ParsingError(message)
        data class IllegalFieldStateError(override val message: String) : ParsingError(message)
        data class MissingFieldError(override val message: String) : ParsingError(message)
    }

    suspend fun parseRaceResult(command: ParseRaceResultCommand): Either<ParsingError, RaceResult>
}
