package com.scalangular.f1.league.v2.generated.transfer.entitys

import com.fasterxml.jackson.annotation.JsonProperty

/**
 *
 * @param raceId
 * @param driverResults
 */
data class RaceResult(

    @JsonProperty("raceId") val raceId: java.util.UUID? = null,

    @JsonProperty("driverResults") val driverResults: kotlin.collections.List<DriverRaceResult>? = null
)
