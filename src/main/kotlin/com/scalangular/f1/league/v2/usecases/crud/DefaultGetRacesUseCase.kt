package com.scalangular.f1.league.v2.usecases.crud

import arrow.core.Either
import arrow.core.right
import com.scalangular.f1.league.v2.adapters.persitence.Mapper
import com.scalangular.f1.league.v2.generated.transfer.entitys.Race
import com.scalangular.f1.league.v2.repositories.RaceRepository
import com.scalangular.f1.league.v2.usecases.crud.GetRacesUseCase.GetRacesCommand
import com.scalangular.f1.league.v2.usecases.crud.GetRacesUseCase.GetRacesError
import com.scalangular.f1.league.v2.entities.Race as EnitityRace

class DefaultGetRacesUseCase(private val raceRepository: RaceRepository) : GetRacesUseCase {
    override fun getRaces(command: GetRacesCommand): Either<GetRacesError, List<Race>> {
        val races: List<EnitityRace> =
            raceRepository.getRacesByLeagueLeagueId(command.leagueId).sortedBy { it.racePositionIndex }

        return Mapper.map(races).right()
    }
}
