package com.scalangular.f1.league.v2.usecases.parsing

interface ValidateAndPlausibilityUseCase {
    data class ValidateUseCaseCommand(
        val export: String
    )

    data class ValidationAndPlausibilityResult(
        val valid: Boolean,
        val messages: List<String>
    )

    fun validateAndPlausibilityCheckExport(command: ValidateUseCaseCommand): ValidationAndPlausibilityResult
}
