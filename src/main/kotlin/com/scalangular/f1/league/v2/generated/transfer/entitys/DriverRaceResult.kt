package com.scalangular.f1.league.v2.generated.transfer.entitys

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull

/**
 *
 * @param raceId
 * @param endPosition Finishing position
 * @param drivenLaps Number of laps completed
 * @param gridPosition Grid position of the car
 * @param bestLapTime Best lap time of the session in seconds
 * @param totalRaceTime Total race time in seconds without penalties
 * @param penaltiesAmount Number of penalties applied to this driver
 * @param penaltiesTime Total penalties accumulated in seconds
 * @param pitstops Number of pit stops made
 * @param driverId
 * @param raceNumber
 * @param raceResultStatus
 */
data class DriverRaceResult(

    @get:NotNull
    @JsonProperty("raceId") val raceId: java.util.UUID?,

    @get:NotNull
    @JsonProperty("endPosition") val endPosition: kotlin.Int,

    @get:NotNull
    @JsonProperty("drivenLaps") val drivenLaps: kotlin.Int,

    @get:NotNull
    @JsonProperty("gridPosition") val gridPosition: kotlin.Int,

    @get:NotNull
    @JsonProperty("bestLapTime") val bestLapTime: kotlin.Float,

    @get:NotNull
    @JsonProperty("totalRaceTime") val totalRaceTime: kotlin.Double,

    @get:NotNull
    @JsonProperty("penaltiesAmount") val penaltiesAmount: kotlin.Int,

    @get:NotNull
    @JsonProperty("penaltiesTime") val penaltiesTime: kotlin.Float,

    @get:NotNull
    @JsonProperty("pitStops") val pitStops: kotlin.Int,

    @JsonProperty("driverId") val driverId: java.util.UUID?,

    @JsonProperty("raceNumber") val raceNumber: Int,

    @JsonProperty("raceResult") val raceResultStatus: F1RaceResultStatus
)

fun com.scalangular.f1.league.v2.entities.DriverRaceResult.toTransferModel(): DriverRaceResult {
    return DriverRaceResult(
        driverId = this.f1Driver.driverId,
        raceId = this.race.raceId,
        gridPosition = this.gridPosition,
        raceResultStatus = this.raceResultStatus,
        raceNumber = this.raceNumber,
        pitStops = this.pitStops,
        penaltiesTime = this.penaltiesTime,
        penaltiesAmount = this.penaltiesAmount,
        totalRaceTime = this.totalRaceTime,
        bestLapTime = this.bestLapTime,
        drivenLaps = this.drivenLaps,
        endPosition = this.endPosition
    )
}
