package com.scalangular.f1.league.v2.usecases.export

import arrow.core.Either
import com.scalangular.f1.league.v2.entities.F1Driver
import com.scalangular.f1.league.v2.entities.Race
import com.scalangular.f1.league.v2.entities.export.PdfExport
import com.scalangular.f1.league.v2.generated.transfer.entitys.RaceResult

interface CreatePdfUseCase {
    data class CreatePdfCommand(
        val raceResult: RaceResult,
        val race: Race,
        val drivers: List<F1Driver>
    )

    sealed class CreatePdfErrors {
        object CouldNotCreatePdfError : CreatePdfErrors()
    }

    suspend fun createPdf(createPdfCommand: CreatePdfCommand): Either<CreatePdfErrors, PdfExport>
}
