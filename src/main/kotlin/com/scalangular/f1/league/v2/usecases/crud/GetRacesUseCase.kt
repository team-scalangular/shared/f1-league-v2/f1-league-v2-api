package com.scalangular.f1.league.v2.usecases.crud

import arrow.core.Either
import com.scalangular.f1.league.v2.generated.transfer.entitys.Race
import java.util.*

interface GetRacesUseCase {
    data class GetRacesCommand(
        val leagueId: UUID
    )

    sealed class GetRacesError

    fun getRaces(command: GetRacesCommand): Either<GetRacesError, List<Race>>
}
