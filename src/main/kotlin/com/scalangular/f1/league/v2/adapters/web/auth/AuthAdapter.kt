package com.scalangular.f1.league.v2.adapters.web.auth

import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse

interface AuthAdapter {
    suspend fun authorizeRequest(
        request: ServerRequest,
        resource: String,
        level: Int,
        block: suspend (ServerRequest) -> ServerResponse
    ): ServerResponse
}
