package com.scalangular.f1.league.v2.adapters.web.auth

import arrow.core.firstOrNone
import com.scalangular.f1.league.v2.ports.output.LoggerDelegate
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.*
import org.springframework.web.reactive.function.server.buildAndAwait

class ScalAngularAuthAdapter(
    @Value("\${authurl:https://auth.scalangular.com/v1/validate}") private val authUrl: String,
    private val client: AuthWebClint
) : AuthAdapter {

    private val logger by LoggerDelegate()

    init {
        logger.info("Initialized ScalAngularAuthAdapter with auth-url $authUrl")
    }

    override suspend fun authorizeRequest(
        request: ServerRequest,
        resource: String,
        level: Int,
        block: suspend (ServerRequest) -> ServerResponse
    ): ServerResponse {
        logger.info("Authorizing request")
        return request.headers().header(HttpHeaders.AUTHORIZATION).firstOrNone { it.trim().isNotEmpty() }.fold(
            {
                logger.info("Unauthorized: no token provided")
                status(HttpStatus.UNAUTHORIZED).buildAndAwait()
            },
            { token -> evaluateToken(token, resource, level, block, request) }
        )
    }

    private suspend fun evaluateToken(
        token: String,
        resource: String,
        level: Int,
        block: suspend (ServerRequest) -> ServerResponse,
        request: ServerRequest
    ): ServerResponse {
        val res: ClientResponse = validateToken(token, resource, level)

        logger.debug(res.statusCode().name)

        return when (res.statusCode()) {
            HttpStatus.OK -> {
                logger.info("Authorization was successful")
                block(request)
            }
            HttpStatus.INTERNAL_SERVER_ERROR -> {
                status(HttpStatus.INTERNAL_SERVER_ERROR).buildAndAwait()
            }
            else -> {
                logger.info("Unauthorized: token is not authorized or invalid")
                status(HttpStatus.UNAUTHORIZED).buildAndAwait()
            }
        }
    }

    private suspend fun validateToken(token: String, resource: String, level: Int): ClientResponse =
        client.authorize(authUrl, token, resource, level)
}
