package com.scalangular.f1.league.v2.entities.export

data class PdfExport(
    val name: String,
    val binary: ByteArray
)
