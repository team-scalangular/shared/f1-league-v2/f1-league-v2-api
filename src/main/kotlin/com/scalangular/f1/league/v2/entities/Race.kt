package com.scalangular.f1.league.v2.entities

import com.scalangular.f1.league.v2.generated.transfer.entitys.F1Circuit
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
data class Race(
    @Id @GeneratedValue(strategy = GenerationType.AUTO) val raceId: UUID?,
    val circuit: F1Circuit,
    val racePositionIndex: Int?,
    val raceDate: LocalDateTime?,
    @ManyToOne val league: League
)
