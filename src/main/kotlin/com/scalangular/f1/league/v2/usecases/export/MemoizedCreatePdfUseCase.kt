package com.scalangular.f1.league.v2.usecases.export

import arrow.core.Either
import com.itextpdf.text.*
import com.itextpdf.text.pdf.PdfPCell
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfWriter
import com.scalangular.f1.league.v2.Memoize
import com.scalangular.f1.league.v2.entities.export.PdfExport
import com.scalangular.f1.league.v2.generated.transfer.entitys.DriverRaceResult
import com.scalangular.f1.league.v2.generated.transfer.entitys.F1RaceResultStatus
import com.scalangular.f1.league.v2.ports.output.LoggerDelegate
import com.scalangular.f1.league.v2.usecases.export.CreatePdfUseCase.CreatePdfCommand
import com.scalangular.f1.league.v2.usecases.export.CreatePdfUseCase.CreatePdfErrors
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import java.util.concurrent.TimeUnit

class MemoizedCreatePdfUseCase : CreatePdfUseCase {

    private val logger by LoggerDelegate()

    override suspend fun createPdf(createPdfCommand: CreatePdfCommand): Either<CreatePdfErrors, PdfExport> =
        withContext(Dispatchers.Default) {
            logger.debug("started building pdf")
            memoizedBuildPdf(createPdfCommand)
        }

    val memoizedBuildPdf by Memoize { createPdfCommand: CreatePdfCommand ->
        buildPdf(createPdfCommand).mapLeft { e ->
            logger.error("Couldn't build pdf: ${e.message}")
            CreatePdfErrors.CouldNotCreatePdfError
        }
    }

    private fun buildPdf(createPdfCommand: CreatePdfCommand): Either<Throwable, PdfExport> =
        Either.catch {
            val document = Document(PageSize.A4.rotate())
            document.setMargins(0f, 1f, 1f, 1f)
            val output = ByteArrayOutputStream()
            PdfWriter.getInstance(document, output)
            document.open()
            document.addAuthor("F1 ScalAngular Api v2")
            document.addCreationDate()

            val title = generateTitle(createPdfCommand)

            document.addTitle(title)

            val font = FontFactory.getFont(FontFactory.HELVETICA, 20f, BaseColor.BLACK)
            val para = Paragraph(
                title,
                font
            )
            para.alignment = Element.ALIGN_LEFT
            para.indentationLeft = 55f
            val table = PdfPTable(10)
            table.widthPercentage = 90f
            addHeaders(
                listOf(
                    "Surname",
                    "Name",
                    "Endposition",
                    "Gridposition",
                    "Best lap",
                    "Total race time",
                    "Driven laps",
                    "Pitstops",
                    "Penalties",
                    "Penaltietime"
                ),
                table
            )

            createPdfCommand.raceResult.driverResults?.sortedBy { result ->
                if (result.raceResultStatus != F1RaceResultStatus.finished) {
                    Int.MAX_VALUE
                } else {
                    result.endPosition
                }
            }?.map { driverResult ->
                buildNamesCells(createPdfCommand, driverResult, table)
                buildEndpositionCell(driverResult, table)
                buildDateCells(driverResult, table)
            }
            document.add(para)
            document.add(Chunk.NEWLINE)
            document.add(table)
            document.close()
            PdfExport(title, output.toByteArray())
        }

    private fun generateTitle(createPdfCommand: CreatePdfCommand) =
        "Race ${createPdfCommand.race.racePositionIndex?.plus(1)}: ${createPdfCommand.race.circuit} ${createPdfCommand.race.raceDate?.toLocalDate()}"

    private fun buildDateCells(
        driverResult: DriverRaceResult,
        table: PdfPTable
    ) {
        if (driverResult.raceResultStatus == F1RaceResultStatus.notClassified) {
            buildDnsRow(table)
        } else {
            buildDefaultRow(driverResult, table)
        }
    }

    private fun buildEndpositionCell(
        driverResult: DriverRaceResult,
        table: PdfPTable
    ) {
        when (driverResult.raceResultStatus) {
            F1RaceResultStatus.finished -> addCell(driverResult.endPosition.toString(), table)
            F1RaceResultStatus.disqualified -> addCell("DSQ", table)
            F1RaceResultStatus.notClassified -> addCell("DNS", table)
            F1RaceResultStatus.retired -> addCell("DNF", table)
            else -> addCell("Unknown", table)
        }
    }

    private fun buildNamesCells(
        createPdfCommand: CreatePdfCommand,
        driverResult: DriverRaceResult,
        table: PdfPTable
    ) {
        val driver = createPdfCommand.drivers.find { driver -> driver.driverId == driverResult.driverId }!!
        addCell(driver.name, table)
        addCell(driver.surename, table)
    }

    private fun buildDefaultRow(
        driverResult: DriverRaceResult,
        table: PdfPTable
    ) {
        addCell(driverResult.gridPosition.toString(), table)
        addCell(formatFastestLap(driverResult.bestLapTime), table)
        addCell(formatFastestLap(driverResult.totalRaceTime.toFloat()), table)
        addCell(driverResult.drivenLaps.toString(), table)
        addCell(driverResult.pitStops.toString(), table)
        addCell(driverResult.penaltiesAmount.toString(), table)
        addCell(driverResult.penaltiesTime.toString(), table)
    }

    private fun buildDnsRow(table: PdfPTable) {
        addCell("-", table)
        addCell("-", table)
        addCell("-", table)
        addCell("-", table)
        addCell("-", table)
        addCell("-", table)
        addCell("-", table)
    }

    private fun addHeaders(headers: List<String>, table: PdfPTable) {
        headers.forEach {
            val header = PdfPCell()
            val headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD)
            header.backgroundColor = BaseColor.LIGHT_GRAY
            header.horizontalAlignment = Element.ALIGN_CENTER
            header.borderWidth = 2f
            header.phrase = Phrase(it, headFont)
            table.addCell(header)
        }
    }

    private fun formatFastestLap(time: Float): String {
        val timeInMillis = (time * 1_000).toLong()

        val minutes = TimeUnit.MILLISECONDS.toMinutes(timeInMillis)
        val seconds = TimeUnit.MILLISECONDS.toSeconds((timeInMillis - minutes * 60 * 1_000))
        val ms = (timeInMillis - minutes * 60 * 1_000) - (seconds * 1_000)

        return String.format(
            "%02d:%02d:%03d", minutes,
            seconds,
            ms
        )
    }

    private fun addCell(element: String?, table: PdfPTable) {
        val phrase = Phrase(element)
        val cell = PdfPCell(phrase)
        cell.verticalAlignment = Element.ALIGN_MIDDLE
        cell.horizontalAlignment = Element.ALIGN_CENTER
        table.addCell(cell)
    }
}
