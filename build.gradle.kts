import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.apache.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.http.Headers.Companion.build
import kotlinx.coroutines.runBlocking
import java.io.FileWriter

buildscript {
    repositories {
        mavenCentral()
        google()
    }
    dependencies {
        classpath("org.springframework.boot:spring-boot-gradle-plugin:2.5.4")
        classpath("io.ktor:ktor-client-core:1.6.3")
        classpath("io.ktor:ktor-client-apache:1.6.3")
        classpath("com.beust:klaxon:5.5")
        classpath("com.fasterxml.jackson.dataformat:jackson-dataformat-xml:2.12.5")
    }
}

group = "com.scalangular"
version = "1.1.0"

repositories {
    maven { url = uri("https://jitpack.io") }
}

tasks.getByName<Jar>("jar") {
    enabled = false
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.register<DeployReviewInstance>("DeployReviewInstance")

tasks.register<DeleteReviewInstance>("DeleteReviewInstance")

abstract class DeleteReviewInstance : DefaultTask() {

    private lateinit var username: String

    @Option(option = "username", description = "Configures the username for portainer.")
    fun setUsername(username: String) {
        this.username = username
    }

    private lateinit var password: String

    @Option(option = "password", description = "Configures the password for portainer.")
    fun setPassword(password: String) {
        this.password = password
    }

    private lateinit var url: String

    @Option(option = "url", description = "Configures the url for portainer.")
    fun setUrl(url: String) {
        this.url = ("prerelease" + Regex("v[0-9]*\\.[0-9]*").find(url)?.value)
    }

    @TaskAction
    fun delete() {
        println("Starting deleting")
        val client = HttpClient(Apache)

        println("authorizing")
        val token = createToken(client, username, password)
        println("authorized")

        val instance = url.replace(".", "-")

        val existing = stackExists(client, token, instance)

        if (existing.first) {
            println("Found existing stack... deleting it")
            deleteStack(client, token, existing.second)
            println("old stack removed")
        }else{
            println("No stack found")
        }
    }

    private fun isNotDeployed(client: HttpClient, token: String, instanceName: String): Boolean {
        return !stackExists(client, token, instanceName).first
    }

    private fun createStack(
        client: HttpClient,
        token: String,
        template: String,
        instanceName: String,
        reviewSwarm: String
    ) {
        val composeFile = File.createTempFile("compose", ".yaml")

        val writer = FileWriter(composeFile)
        writer.write(template)
        writer.close()

        runBlocking {
            val response = client.submitFormWithBinaryData<HttpResponse>(
                url = "https://portainer.scalangular.com/api/stacks?type=1&method=file&endpointId=1",
                formData = formData {
                    append("Name", instanceName)
                    append("EndpointID", 1)
                    append("SwarmID", reviewSwarm)
                    append("file", composeFile.readBytes(), build {
                        append(HttpHeaders.ContentType, "text/x-yaml")
                        append(HttpHeaders.ContentDisposition, "filename=compose.yaml")
                    })
                }
            ) {
                header(HttpHeaders.Authorization, token)
            }

            if (response.status != io.ktor.http.HttpStatusCode.Companion.OK)
                throw Exception("Error while creating stack")
        }
    }

    private fun deleteStack(client: io.ktor.client.HttpClient, token: String, id: Int) {
        runBlocking {
            val result: io.ktor.client.statement.HttpResponse =
                client.delete("https://portainer.scalangular.com/api/stacks/$id") {
                    header(HttpHeaders.Authorization, token)
                }

            if (!(result.status.value in 200..299))
                throw Exception("Error while deleting old stack")
        }
    }

    private fun stackExists(client: HttpClient, token: String, instanceName: String): Pair<Boolean, Int> {
        return runBlocking {
            val result: io.ktor.client.statement.HttpResponse =
                client.get("https://portainer.scalangular.com/api/stacks") {
                    header(HttpHeaders.Authorization, token)
                }

            if (result.status != io.ktor.http.HttpStatusCode.Companion.OK)
                throw Exception("Error while checking if stack already existed")

            val tree = com.fasterxml.jackson.databind.ObjectMapper().readTree(result.receive<String>())

            val exists = tree.elements().asSequence().filter {
                it["Name"]?.asText() == instanceName
            }.any()

            val number = if (exists) {
                tree.elements().asSequence().filter {
                    it["Name"]?.asText() == instanceName
                }.first()["Id"].asInt()
            } else {
                -1
            }

            exists to number
        }
    }

    private fun loadTemplate(
        instanceName: String,
        domainName: String,
        image: String,
        dbUrl: String,
        dbUser: String,
        dbPassword: String
    ): String {
        return File("scripts/templates/compose-template.yaml").readText(Charsets.UTF_8)
            .replace("\${instance_name}", instanceName)
            .replace("\${domain_name}", domainName)
            .replace("\${image}", image)
            .replace("\${db_url}", dbUrl)
            .replace("\${db_user}", dbUser)
            .replace("\${db_password}", dbPassword)
    }

    data class JWT(val jwt: String)

    private fun createToken(client: HttpClient, username: String, password: String): String =
        runBlocking {
            val response: io.ktor.client.statement.HttpResponse =
                client.post("https://portainer.scalangular.com/api/auth") {
                    header(io.ktor.http.HttpHeaders.ContentType, "application/json")
                    body = """{"Username": "$username", "Password": "$password"}"""
                }

            val token = response.receive<String>()

            if (response.status != io.ktor.http.HttpStatusCode.Companion.OK)
                throw Exception("Error while creating token")

            val json = com.beust.klaxon.Klaxon().parse<JWT>(token)

            json!!.jwt
        }

}


abstract class DeployReviewInstance : DefaultTask() {

    private lateinit var username: String

    @Option(option = "username", description = "Configures the username for portainer.")
    fun setUsername(username: String) {
        this.username = username
    }

    private lateinit var password: String

    @Option(option = "password", description = "Configures the password for portainer.")
    fun setPassword(password: String) {
        this.password = password
    }

    private lateinit var url: String

    @Option(option = "url", description = "Configures the url for portainer.")
    fun setUrl(url: String) {
        this.url = ("prerelease" + Regex("v[0-9]*\\.[0-9]*").find(url)?.value)
    }

    private lateinit var dbUrl: String

    @Option(option = "dbUrl", description = "Configures the dbUrl for db.")
    fun setdbUrl(dbUrl: String) {
        this.dbUrl = dbUrl
    }

    private lateinit var dbPassword: String

    @Option(option = "dbPassword", description = "Configures the dbPassword for db.")
    fun setDbPassword(dbPassword: String) {
        this.dbPassword = dbPassword
    }

    private lateinit var dbUser: String

    @Option(option = "dbUser", description = "Configures the dbUser for db.")
    fun setdbUser(dbUser: String) {
        this.dbUser = dbUser
    }

    private lateinit var image: String

    @Option(option = "image", description = "Configures the image for portainer.")
    fun setimage(image: String) {
        this.image = image
    }

    private lateinit var reviewSwarm: String

    @Option(option = "reviewSwarm", description = "Configures the reviewSwarm for portainer.")
    fun setreviewSwarm(reviewSwarm: String) {
        this.reviewSwarm = reviewSwarm
    }

    @TaskAction
    fun deploy() {
        println("Starting deployment to review instance")
        val client = HttpClient(Apache)

        println("authorizing")
        val token = createToken(client, username, password)
        println("authorized")

        println("loading template")
        val updatedUrl = "api.$url.scalangular.com"

        val instance = url.replace(".", "-")

        val template = loadTemplate(instance, updatedUrl, image, dbUrl, dbUser, dbPassword)
        println("loaded template successfully")

        val existing = stackExists(client, token, this.url)

        if (existing.first) {
            println("Removing existing stack")
            deleteStack(client, token, existing.second)
            println("old stack removed")
        }

        println("Creating stack")
        createStack(client, token, template, instance, reviewSwarm)

        Thread.sleep(2000)

        if (isNotDeployed(client, token, instance)) {
            throw Exception("Something went wrong during the deployment")
        }

        println("url: https://$updatedUrl")
        println("stack created")
    }

    private fun isNotDeployed(client: HttpClient, token: String, instanceName: String): Boolean {
        return !stackExists(client, token, instanceName).first
    }

    private fun createStack(
        client: HttpClient,
        token: String,
        template: String,
        instanceName: String,
        reviewSwarm: String
    ) {
        val composeFile = File.createTempFile("compose", ".yaml")

        val writer = FileWriter(composeFile)
        writer.write(template)
        writer.close()

        runBlocking {
            val response = client.submitFormWithBinaryData<HttpResponse>(
                url = "https://portainer.scalangular.com/api/stacks?type=1&method=file&endpointId=1",
                formData = formData {
                    append("Name", instanceName)
                    append("EndpointID", 1)
                    append("SwarmID", reviewSwarm)
                    append("file", composeFile.readBytes(), build {
                        append(HttpHeaders.ContentType, "text/x-yaml")
                        append(HttpHeaders.ContentDisposition, "filename=compose.yaml")
                    })
                }
            ) {
                header(HttpHeaders.Authorization, token)
            }

            if (response.status != io.ktor.http.HttpStatusCode.Companion.OK)
                throw Exception("Error while creating stack")
        }
    }

    private fun deleteStack(client: io.ktor.client.HttpClient, token: String, id: Int) {
        runBlocking {
            val result: io.ktor.client.statement.HttpResponse =
                client.delete("https://portainer.scalangular.com/api/stacks/$id") {
                    header(HttpHeaders.Authorization, token)
                }

            if (!(result.status.value in 200..299))
                throw Exception("Error while deleting old stack")
        }
    }

    private fun stackExists(client: HttpClient, token: String, instanceName: String): Pair<Boolean, Int> {
        return runBlocking {
            val result: io.ktor.client.statement.HttpResponse =
                client.get("https://portainer.scalangular.com/api/stacks") {
                    header(HttpHeaders.Authorization, token)
                }

            if (result.status != io.ktor.http.HttpStatusCode.Companion.OK)
                throw Exception("Error while checking if stack already existed")

            val tree = com.fasterxml.jackson.databind.ObjectMapper().readTree(result.receive<String>())

            val exists = tree.elements().asSequence().filter {
                it["Name"]?.asText() == instanceName
            }.any()

            val number = if (exists) {
                tree.elements().asSequence().filter {
                    it["Name"]?.asText() == instanceName
                }.first()["Id"].asInt()
            } else {
                -1
            }

            exists to number
        }
    }

    private fun loadTemplate(
        instanceName: String,
        domainName: String,
        image: String,
        dbUrl: String,
        dbUser: String,
        dbPassword: String
    ): String {
        return File("scripts/templates/compose-template.yaml").readText(Charsets.UTF_8)
            .replace("\${instance_name}", instanceName)
            .replace("\${domain_name}", domainName)
            .replace("\${image}", image)
            .replace("\${db_url}", dbUrl)
            .replace("\${db_user}", dbUser)
            .replace("\${db_password}", dbPassword)
    }

    data class JWT(val jwt: String)

    private fun createToken(client: HttpClient, username: String, password: String): String =
        runBlocking {
            val response: io.ktor.client.statement.HttpResponse =
                client.post("https://portainer.scalangular.com/api/auth") {
                    header(io.ktor.http.HttpHeaders.ContentType, "application/json")
                    body = """{"Username": "$username", "Password": "$password"}"""
                }

            val token = response.receive<String>()

            if (response.status != io.ktor.http.HttpStatusCode.Companion.OK)
                throw Exception("Error while creating token")

            val json = com.beust.klaxon.Klaxon().parse<JWT>(token)

            json!!.jwt
        }

}


plugins {
    val kotlinVersion = "1.5.30-RC"
    id("info.solidsoft.pitest") version "1.5.1"
    id("org.jetbrains.kotlin.jvm") version kotlinVersion
    id("org.jetbrains.kotlin.plugin.jpa") version kotlinVersion
    id("org.jetbrains.kotlin.plugin.spring") version kotlinVersion
    id("org.springframework.boot") version "2.5.4"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    kotlin("plugin.allopen") version "1.5.10"
}

ext["log4j2.version"] = "2.15.0"

dependencies {
    val kotlinxCoroutinesVersion = "1.5.2-native-mt"
    val arrowVersion = "0.13.2"
    val kotestVersion = "4.6.3"

    implementation("io.ktor:ktor-client-core:1.6.3")
    implementation("io.ktor:ktor-client-apache:1.6.3")
    implementation("com.beust:klaxon:5.5")

    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinxCoroutinesVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor:$kotlinxCoroutinesVersion")

    implementation("org.springframework.boot:spring-boot-starter-data-jpa:2.5.4")
    implementation("org.springframework.boot:spring-boot-starter-webflux:2.5.4")
    implementation("org.springframework.boot:spring-boot-starter-cache:2.5.4")
    implementation("org.springframework.boot:spring-boot-starter-actuator")

    implementation("io.micrometer:micrometer-registry-prometheus:1.7.5")

    implementation("javax.servlet:javax.servlet-api:4.0.1")

    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:2.12.5")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-xml:2.12.5")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.12.5")

    implementation("io.arrow-kt:arrow-core:$arrowVersion")
    implementation("io.arrow-kt:arrow-fx-coroutines:$arrowVersion")

    implementation("org.postgresql:postgresql:42.2.23")



    implementation("ch.qos.logback:logback-core:1.2.6")
    implementation("ch.qos.logback:logback-classic:1.2.6")
    implementation("org.slf4j:slf4j-api:1.7.32")

    implementation("com.itextpdf:itextpdf:5.5.13.2")

    implementation("org.junit.jupiter:junit-jupiter:5.8.0")
    runtimeOnly("com.h2database:h2:1.4.200")


    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:1.5.30")
    testImplementation(platform("org.junit:junit-bom:5.7.0"))
    testImplementation("io.kotest:kotest-runner-junit5:$kotestVersion")
    testImplementation("io.kotest:kotest-assertions-core:$kotestVersion")
    testImplementation("io.kotest.extensions:kotest-assertions-arrow:1.0.3")
    testImplementation("io.kotest.extensions:kotest-extensions-spring:1.0.1")
    testImplementation("org.springframework.boot:spring-boot-starter-test:2.5.4") {
        exclude(module = "junit")
    }
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:$kotlinxCoroutinesVersion")
}

repositories {
    maven { url = uri("https://repo1.maven.org/maven2") }
    maven { url = uri("https://repo.spring.io/snapshot") }
    maven { url = uri("https://repo.spring.io/milestone") }
}
