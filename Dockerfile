FROM gradle:7.2.0-jdk11-hotspot AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle clean build --no-daemon

# Container setup --------
FROM adoptopenjdk/openjdk11:jdk-11.0.1.13-alpine-slim
# Creating user
ENV APPLICATION_USER 1033
RUN adduser -D -g '' $APPLICATION_USER

# Giving permissions
RUN mkdir /app
RUN mkdir /app/resources
RUN chown -R $APPLICATION_USER /app
RUN chmod -R 755 /app

# Setting user to use when running the image
USER $APPLICATION_USER

# Copying needed files
COPY --from=build /home/gradle/src/build/libs/*.jar /app/F1LeagueV2.jar
CMD [ "java", "-jar", "-Djava.security.egd=file:/dev/./urandom", "/app/F1LeagueV2.jar" ]